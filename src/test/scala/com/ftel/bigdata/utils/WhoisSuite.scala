package com.ftel.bigdata.utils

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.ftel.bigdata.whois.CzParser
import com.ftel.bigdata.whois.InternicParser
import com.ftel.bigdata.whois.NuParser
import com.ftel.bigdata.whois.VnParser
import com.ftel.bigdata.whois.OrgParser
import com.ftel.bigdata.whois.RuParser
import com.ftel.bigdata.whois.ItParser

@RunWith(classOf[JUnitRunner])
class WhoisSuite extends FunSuite {
  test("Test CzParser") {
    val parser = new CzParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    assert(whois.isValid() == true)
  }
  
  test("Test InternicParser") {
    val parser = new InternicParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    assert(whois.isValid() == true)
  }
  
  test("Test ItParser") {
    val parser = new ItParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    assert(whois.isValid() == true)
  }
  
  test("Test NuParser") {
    val parser = new NuParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    assert(whois.isValid() == true)
  }
  
  test("Test OrgParser") {
    val parser = new OrgParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    assert(whois.isValid() == true)
  }
  
  test("Test RuParser") {
    val parser = new RuParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    assert(whois.isValid() == true)
  }
  
  test("Test VnParser") {
    val parser = new VnParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    assert(whois.isValid() == true)
  }
}
