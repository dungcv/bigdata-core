package com.ftel.bigdata.utils

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class DomainUtilSuite extends FunSuite {
  test("Test Get Second Domain") {
    assert(DomainUtil.split("storage.googleapis.com").second == "storage.googleapis.com")
    assert(DomainUtil.split("google.com.vn").second == "google.com.vn")
    assert(DomainUtil.split("android.clients.google.com").second == "google.com")
    assert(DomainUtil.split("vntrip.vn").second == "vntrip.vn")
    assert(DomainUtil.split("service.vntrip.vn").second == "vntrip.vn")
    assert(DomainUtil.split("prod-clientlog.fastly.newssuite.sinfony.ne.jp").second == "sinfony.ne.jp")
    assert(DomainUtil.split("ne.jp").second == null)
    assert(DomainUtil.split("ec2-79-125-123-54.eu-west-1.compute.amazonaws.com").second == "ec2-79-125-123-54.eu-west-1.compute.amazonaws.com")
    assert(DomainUtil.split("http://www.zoyanailpolish.blogspot.com").second == "zoyanailpolish.blogspot.com")
    assert(DomainUtil.split("http://google.com").second == "google.com")
    assert(DomainUtil.split("www.ne.jp").second == "www.ne.jp")
    assert(DomainUtil.split("carmudi.com.bd").second == "carmudi.com.bd")
    assert(DomainUtil.split("xyz.abc.kawasaki.jp").second == "xyz.abc.kawasaki.jp")
    assert(DomainUtil.split("http://www.alipay.com/").second == "alipay.com")
  }

  test("Check Domain is valid") {
    assert(DomainUtil.isValid("storage.googleapis.com") == true)
    assert(DomainUtil.isValid("hnrzdfbqyth") == false)
    assert(DomainUtil.isValid("s3-external-1.amazonaws.com") == false)
    assert(DomainUtil.isValid(".com") == false)
    assert(DomainUtil.isValid("googleapis.com") == false)
    assert(DomainUtil.isValid("com") == false)
    assert(DomainUtil.isValid("b-c.men") == true)
    assert(DomainUtil.isValid("-") == false)
    assert(DomainUtil.isValid("empty") == false)
    assert(DomainUtil.isValid("tunkhiitbhrwjvwemryu.bit") == true)
    
  }
  
  test("Nomalize domain") {
    assert(DomainUtil.nomalize("www.ne.jp") == "www.ne.jp")
    assert(DomainUtil.nomalize("   www.ne.jp") == "www.ne.jp")
    assert(DomainUtil.nomalize("ne.jp") == "ne.jp")
    assert(DomainUtil.nomalize("http://www.ne.jp") == "www.ne.jp")
    assert(DomainUtil.nomalize("https://www.ne.jp") == "www.ne.jp")
  }
}
