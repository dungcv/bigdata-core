package com.ftel.bigdata.utils

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import com.ftel.bigdata.utils.LangDetectUtil.getLanguageCode

@RunWith(classOf[JUnitRunner])
class LangDetectUtilSuite extends FunSuite {
  test("Test Language detect") {
    assert(getLanguageCode("小松（中国）投资有限公司专业生产小松挖掘机、小松装载机、小松矿用车等工程机械") == "zh")
    assert(getLanguageCode("Công ty lọc hóa dầu Bình Sơn") == "vi")
    assert(getLanguageCode("Az Externet Magyarország legnagyobb alternatív távközlési szolgáltatója. Lakossági és üzleti ügyfeleknek egyaránt kínál internet") == "hu")
    assert(getLanguageCode("View more than 20 million economic indicators for 196 countries") == "en")
    assert(getLanguageCode("宮崎市のケーブルテレビ局。充実のチャンネルラインナップ！ さらにインターネット接続サービス、ケーブルプラス電話など快適でお得なサービスをご提供します") == "ja")
    assert(getLanguageCode("Professionelle Filmproduktion für Unternehmen und Agenturen. Auf höchstem Niveau. Mit Herzblut. Aus Karlsruhe") == "de")
  }
}
