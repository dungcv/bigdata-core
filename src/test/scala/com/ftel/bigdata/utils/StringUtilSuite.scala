package com.ftel.bigdata.utils

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StringUtilSuite extends FunSuite {
  test("Test replace all space") {
    assert(StringUtil.replaceAllWhiteSpace("there  are   many spaces     in between  word.\t TAB") == "there are many spaces in between word. TAB")
    assert(StringUtil.replaceAllWhiteSpace("     there  are   many spaces     in between  word.\t TAB     ") == "there are many spaces in between word. TAB")
  }

  test("Test remove all space") {
    assert(StringUtil.removeAllWhiteSpace("there  are   many spaces     in between  word.\t TAB") == "therearemanyspacesinbetweenword.TAB")
    assert(StringUtil.removeAllWhiteSpace("     there  are   many spaces     in between  word.\t TAB     ") == "therearemanyspacesinbetweenword.TAB")
  }
}
