package com.ftel.bigdata.utils

/**
 * Number Utilities.
 */
object NumberUtil {
  
  def toInt(s: String): Int = toIntOption(s).getOrElse(0)
  def toInt(s: String, default: Int): Int = toIntOption(s).getOrElse(default)
  def toLong(s: String): Long = toLongOption(s).getOrElse(0)
  def toLong(s: String, default: Long): Long = toLongOption(s).getOrElse(default)
  
  private def toIntOption(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: NumberFormatException => None
    }
  }

  private def toLongOption(s: String): Option[Long] = {
    try {
      Some(s.toDouble.toLong)
    } catch {
      case e: NumberFormatException => None
    }
  }
  
  def format(number: Int, numDigitDisplay: Int): String = {
    //f"$number%0${numZeroBefore}d" // Error => can't use a variable behind %0
    //f"$number%02d"
    number.toString().reverse.padTo(numDigitDisplay, "0").reverse.mkString
  }
  
  def doubleWithoutE(number: Double): String = {
    f"${number}%.1f"
  }
  
  def main(args: Array[String]) {
    println(format(123,4))
  }
}