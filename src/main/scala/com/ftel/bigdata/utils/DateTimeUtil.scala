package com.ftel.bigdata.utils

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTimeZone

object DateTimeUtil {
  val YMD = "yyyy-MM-dd"
  val ES_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS Z"
  val FULL_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS Z"
  val AMIRSHEMESH_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS"
  val TIMEZONE_HCM = "Asia/Ho_Chi_Minh"
  val GMT = "GMT"
  val FULL_FORMAT2 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
  
  def create(seconds: Long): DateTime = new DateTime(seconds * 1000L).withZone(DateTimeZone.forID(TIMEZONE_HCM))
  def create(dateAsString: String, pattern: String): DateTime = {
    DateTime.parse(dateAsString, DateTimeFormat.forPattern(pattern)).withZone(DateTimeZone.forID(TIMEZONE_HCM))
  }
  
  def now = new DateTime()
  
  def test() {
    val timestamp = 1520272740254L
    val date = DateTimeUtil.create(timestamp / 1000)
    println(date)
    //println(date.toString("HH"))
    
    
    val dateString = "2018-03-06T12:05:43.588Z"
    
    val d1 = DateTimeUtil.create(dateString, FULL_FORMAT2)
    val t = d1.getMillis
    
    val d2 = DateTimeUtil.create(t / 1000)
    
    println(t) // 1520312743570
    println(d1)
    println(d2)
  }
  
  def main(args: Array[String]) {
    test()
  }
}