package com.ftel.bigdata.utils

import org.apache.commons.net.whois.WhoisClient
import java.net.SocketException
import java.io.IOException
import java.net.Socket
import java.io.InputStreamReader
import java.io.BufferedReader
import java.io.PrintWriter
import javax.net.SocketFactory
import com.ftel.bigdata.whois.Whois

/**
 * Link Lookup:
 * 	http://www.domaininformation.de/whois/google.com
 *  https://www.whoisxmlapi.com/?domainName=vnexpress.net&outputFormat=json
 *  https://www.whois.com/whois/vnexpress.net
 *  https://dig.whois.com.au/whois/vnexpress.net
 *  https://www.iana.org/whois?q=vnexpress.net
 *  https://reports.internic.net/cgi/whois?whois_nic=vnexpress.net&type=domain
 * 	
 */
object WhoisUtil {

  val DOMAIN_PARAMETER = "@DOMAIN@"
  val DOMAIN_INFORMATION = s"http://www.domaininformation.de/whois/${DOMAIN_PARAMETER}"
  val WHOIS_XML_API = s"https://www.whoisxmlapi.com/?domainName=${DOMAIN_PARAMETER}&outputFormat=json"
  val WHOIS = s"https://www.whois.com/whois/${DOMAIN_PARAMETER}"
  val DIG_WHOIS = s"https://dig.whois.com.au/whois/${DOMAIN_PARAMETER}"
  val IANA = s"https://www.iana.org/whois?q=${DOMAIN_PARAMETER}"
  val REPORT_INTERNIC =  s"https://reports.internic.net/cgi/whois?whois_nic=${DOMAIN_PARAMETER}&type=domain"
  
  val WHOIS_SERVICE = Array(DOMAIN_INFORMATION, WHOIS_XML_API, WHOIS, DIG_WHOIS, IANA)

  /**
   * Get whois domain from web with content is HTML
   */
  def whoisService(domain: String, proxyHost: String, proxyPort: Int): Whois = {
    val url = DOMAIN_INFORMATION.replace(WhoisUtil.DOMAIN_PARAMETER, domain)
    val content = HttpUtil.getContent(url, proxyHost, proxyPort)
    val beginIndex = content.indexOf("<pre>") + 5
    val endIndex = content.indexOf("</pre>")
    if (endIndex > beginIndex && beginIndex > 0) {
      Whois(content.substring(beginIndex, endIndex), domain)
    } else {
      Whois(content, domain)
    }
  }
  
  def main(args: Array[String]) {
    println(whoisService("vnexpress.net", "172.30.45.220", 80))
    println(whoisService("tin.it", "172.30.45.220", 80))
    //println("================")
    //println(whoisService("google.com", 0, "172.30.45.220", 80))
  }
}