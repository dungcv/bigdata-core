package com.ftel.bigdata.utils

import com.google.common.net.InternetDomainName

//package com.ftel.bigdata.utils
//
//import scala.collection.JavaConversions.asScalaBuffer
//import scala.util.matching.Regex
//import com.google.common.net.InternetDomainName
//
//case class SplitHost(subdomain: String, domain: String, tld: String)
//
//object SplitHost {
//  //val LenientURLParse = new Regex("""^(\S+:)?//(.*@)?([^/:]+)""", "scheme", "user", "domain")
//  val LenientURLParse = new Regex("""^([^/:]+)""", "domain")
//  val IP = new Regex("""^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$""")
//
//  def fromURL(url: String): SplitHost = {
//    val fullDomain = LenientURLParse findFirstMatchIn (url) map (_.group("domain")) getOrElse ("")
//    val extracted = try {
//      Some(InternetDomainName fromLenient (fullDomain))
//    }
//    catch {
//      case iex: IllegalArgumentException => None
//    }
//
//    extracted match {
//      case Some(idn) => {
//        val suffix = idn.publicSuffix
//        val subdomainAndDomain = idn.parts dropRight (suffix.parts.size)
//        val domain = subdomainAndDomain.lastOption getOrElse ("")
//        val subdomain = subdomainAndDomain.init mkString (".")
//        SplitHost(subdomain, domain, suffix.name)
//      }
//      case _ => {
//        if (isIP(fullDomain)) {
//          SplitHost("", fullDomain, "")
//        }
//        else {
//          val subdomainAndDomain = fullDomain.split(".").toList
//          val domain = subdomainAndDomain.lastOption getOrElse ("")
//          val subdomain = subdomainAndDomain.init mkString (".")
//          SplitHost(subdomain, domain, "")
//        }
//      }
//    }
//  }
//
//  private def isIP(possibleIP: String) = IP.findFirstIn(possibleIP).isDefined
//}
//
object TLDExtract  {
//  def splitFromArgs() {
//    val url = "a.com.vn"
//    val split = SplitHost.fromURL(url)
//    println(split)
//    println(split.productIterator mkString (" "))
//  }
//
//  splitFromArgs()
  
  def main(args: Array[String]) {
//  val domain = "www.storage.googleapis.com"
//  val a = InternetDomainName.from(domain)// (url)
//  println(a.topPrivateDomain())
//  val b = a.publicSuffix()
//  println(b)
//  println(a)
//    val domain = "ne.jp"
//    val domain = ".com"
    val domain = "carmudi.com.bd"
    
//    val domain = "com.vn"
//    val domain = "google.com.vn"
    //val domain = "google.com.vn"
    //val domain = "storage.googleapis.com"
//    val domain = "s3.amazonaws.com"
    //val domain = "google.com"
//    val domain = "ec2-79-125-123-54.eu-west-1.compute.amazonaws.com"
//    val domain = "zoyanailpolish.blogspot.com"
    //val domain = "http://www.zoyanailpolish.blogspot.com"
//    val mainDomain = DomainUtil.extract(domain).getMain()
    //println(SecondLDExtractor.extract(domain))
    val time0 = System.currentTimeMillis()
    //println("BEGIN")
    //println(DomainUtil.split(domain))
    println(DomainUtil.split(domain))
    val time1 = System.currentTimeMillis()
    
    println("Time: " + (time1 - time0))
//    println(mainDomain)
  }
}
