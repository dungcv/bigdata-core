package com.ftel.bigdata.utils

import java.io.File
import scala.util.Try
import java.nio.file.Files
import scala.reflect.io.Path
import java.nio.charset.Charset
import java.nio.file.Paths
import java.io.BufferedWriter
import java.io.FileWriter
import scala.io.Source
import org.apache.commons.io.FileUtils
import java.io.IOException

object FileUtil {
  
  /**
   * Read content form fileName
   */
  def readLines(fileName: String): List[String] = {
    val file = Source.fromFile(fileName)
    val res = file.getLines.toList
    // Close file before return result to avoid error "too many file opened"
    file.close()
    res
  }

  /**
   * Read content form file in resource path
   */
  def readResource(inPath: String): List[String] = {
    val in = FileUtil.getClass.getResourceAsStream(inPath)
    val buffer = scala.io.Source.fromInputStream(in)
    buffer.getLines().toList
  }

  /**
   * Write content to file. 
   * If path to file don't exist => Exception: No such file or directory
   */
  def write(fileName: String, content: String) {
    try {
      val file = new File(fileName)
      val bw = new BufferedWriter(new FileWriter(file))
      bw.write(content)
      bw.close()
    } catch {
      case e: IOException => e.printStackTrace()
    }
  }

  /**
   * Write content to file. 
   * If path to file don't exist => Exception: No such file or directory
   */
  def write(fileName: String, content: Array[Byte]) {
    Files.write(new File(fileName).toPath(), content)
  }

  /**
   * Move from oldPath to newPath
   */
  def move(oldPath: String, newPath: String): Boolean = {
    Try(new File(oldPath).renameTo(new File(newPath))).getOrElse(false)
  }

  /**
   * Copy from oldPath to newPath
   */
  def copy(oldPath: String, newPath: String) = {
    Files.copy(new File(oldPath).toPath(), new File(newPath).toPath())
  }

  /**
   * Delete file or directory
   */
  def delete(filePath: String): Boolean = {
    val path: Path = Path(filePath)
    Try(path.deleteRecursively()).getOrElse(false)
  }

  /**
   * Delete directory empty
   */
  def deleteDirectoryIfEmpty(dir: File): Unit = {
    if (dir.isDirectory() && dir.list().isEmpty) {
      FileUtils.deleteDirectory(dir)
    }
  }
  
  /**
   * Append more text to end file.
   * Exception if file don't exist
   */
  def append(line: String, fileName: String) {
    val fw = new FileWriter(fileName, true)
    try {
      fw.write(line)
      fw.write("\n")
    } finally fw.close()
  }
  
  /**
   * Get all file in directory.
   * Return name fo File, don't full path
   */
  def getListOfFiles(dir: String): List[String] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).map(x => x.getName).toList
    } else {
      List[String]()
    }
  }
  
  /**
   * Get all sub directory in directory.
   * Return name fo File, don't full path
   */
  def getListOfDir(dir: String): List[String] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isDirectory).map(x => x.getName).toList
    } else {
      List[String]()
    }
  }
  
  /**
   * Get all file and sub directory in directory.
   * Return name fo File, don't full path
   */
  def getList(dir: String): List[String] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.map(x => x.getName).toList
    } else {
      List[String]()
    }
  }
   
  def isExist(path: String): Boolean = {
    new java.io.File(path).exists
  }
  
  
  /**
   * Reference: https://github.com/gitbucket/gitbucket/blob/master/src/main/scala/gitbucket/core/util/FileUtil.scala
   */
  //  def getMimeType(name: String): String =
  //    defining(new Tika()){ tika =>
  //      tika.detect(name) match {
  //        case null     => "application/octet-stream"
  //        case mimeType => mimeType
  //      }
  //    }
  //
  //  def getContentType(name: String, bytes: Array[Byte]): String = {
  //    defining(getMimeType(name)){ mimeType =>
  //      if(mimeType == "application/octet-stream" && isText(bytes)){
  //        "text/plain"
  //      } else {
  //        mimeType
  //      }
  //    }
  //  }
  //
  //  def isImage(name: String): Boolean = getMimeType(name).startsWith("image/")
  //
  //  def isUploadableType(name: String): Boolean = mimeTypeWhiteList contains getMimeType(name)
  //
  //  def isLarge(size: Long): Boolean = (size > 1024 * 1000)
  //
  //  def isText(content: Array[Byte]): Boolean = !content.contains(0)
  //
  //  def generateFileId: String = System.currentTimeMillis + Random.alphanumeric.take(10).mkString
  //
  //  def getExtension(name: String): String =
  //    name.lastIndexOf('.') match {
  //      case i if(i >= 0) => name.substring(i + 1)
  //      case _ => ""
  //    }
  //
  //  def withTmpDir[A](dir: File)(action: File => A): A = {
  //    if(dir.exists()){
  //      FileUtils.deleteDirectory(dir)
  //    }
  //    try {
  //      action(dir)
  //    } finally {
  //      FileUtils.deleteDirectory(dir)
  //    }
  //  }
  //
  //  val mimeTypeWhiteList: Array[String] = Array(
  //      "application/pdf",
  //      "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  //      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  //      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  //      "image/gif",
  //      "image/jpeg",
  //      "image/png",
  //      "text/plain")
  //
  //  def getLfsFilePath(owner: String, repository: String, oid: String): String =
  //    Directory.getLfsDir(owner, repository) + "/" + oid
  //
  //  def readableSize(size: Long): String = FileUtils.byteCountToDisplaySize(size)

  

//  def write(fileName: String, content: String) {
//
//    //val file = Paths.get(fileName);
//    //Files.write(file, content.toByte, Charset.forName("UTF-8"));
//    // FileWriter
//    val file = new File(fileName)
//    val path = Paths.get(fileName)
//    //Files.createFile(path)
//
//    //file.createNewFile()
//    val bw = new BufferedWriter(new FileWriter(file))
//    bw.write(content)
//    bw.close()
//  }

  
}