package com.ftel.bigdata.utils

import org.apache.spark.SparkContext
import scala.collection.immutable.HashSet

case class DomainSplit(full: String, second: String, tld: String) {
  override def toString = full + "\t" + second + "\t" + tld
}

object DomainUtil {

  private val HTTP = "http://"
  private val HTTPS = "https://"
//  private val WWW = "www."

  private val TOP_LEVEL_DOMAIN = HashSet() ++ getTopLevelDomain()
  private val TOP_LEVEL_DOMAIN_ONE = TOP_LEVEL_DOMAIN.filter(x => x.split("\\.").length == 1)
  private val TOP_LEVEL_DOMAIN_TWO = TOP_LEVEL_DOMAIN.filter(x => x.split("\\.").length == 2)
  private val TOP_LEVEL_DOMAIN_THREE = TOP_LEVEL_DOMAIN.filter(x => x.split("\\.").length == 3)
  private val TOP_LEVEL_DOMAIN_FOUR = TOP_LEVEL_DOMAIN.filter(x => x.split("\\.").length == 4)
  private val TOP_LEVEL_DOMAIN_FIVE = TOP_LEVEL_DOMAIN.filter(x => x.split("\\.").length == 5)

  def split(domain: String): DomainSplit = {
    val full = nomalize(domain)
    val tld = getTopLevelDomain(full)
    val second = getSecondTopLevel(full, tld)
    DomainSplit(full, second, tld)
  }
  
  def extract(domain: String): DomainSplit = {
    split(domain)
  }
  
  def nomalize(domain: String): String = {
//    val lowerCase = domain.toLowerCase().trim()
//    val withoutHttp = if (lowerCase.startsWith(HTTP)) lowerCase.substring(HTTP.length()) else lowerCase
//    val withoutHttps = if (withoutHttp.startsWith(HTTPS)) withoutHttp.substring(HTTPS.length()) else withoutHttp
//    //if (withoutHttps.startsWith(WWW)) withoutHttps.substring(4) else withoutHttps
//    withoutHttps
    val domainNomalize = getProtocol(domain)._2
    val arr = domainNomalize.split("/")
    if (arr.length > 0) arr(0) else domainNomalize
  }
  
  def getProtocol(url: String): (String, String) = {
    val lowerCase = url.toLowerCase().trim()
    if (lowerCase.startsWith(HTTPS)) "https" -> lowerCase.substring(HTTPS.length())
    else {
      if (lowerCase.startsWith(HTTP)) "http" -> lowerCase.substring(HTTP.length())
      else "" -> lowerCase
    }
  }

  private def getDomainFromUrl(url: String): String = {
    val nommalizeUrl = nomalize(url)
    val index = nommalizeUrl.indexOf("/")
    if (index > 0) nommalizeUrl.substring(0, index) else nommalizeUrl
  }

//  private def getTopLevel(domain: String): String = {
//    val arr = domain.toLowerCase().trim().split("\\.")
//    arr(arr.size - 1)
//  }

//  private def getSecondTopLevel(domain: String): String = {
//    val tld = getTopLevelDomain(domain)
//    getSecondTopLevel(domain, tld)
//  }

  private def getSecondTopLevel(domain: String, tld: String): String = {
    if (tld == null || domain == tld) null else {
        val topLevel = "." + tld
        //val domainRemoveTld = domain.replace("." + tld, "") // How to this process will wrong with domain statics.vntrip.vn
        //val domainRemoveTld = domain.substring(0, domain.lastIndexOf(".")) // How to this process will wrong with domain have tld 2 part such as: google.com.vn
        val domainRemoveTld = domain.substring(0, domain.lastIndexOf(topLevel))
        val arr = domainRemoveTld.toLowerCase().trim().split("\\.")
        arr(arr.size - 1) + topLevel
      }
  }

  private def getTopLevelDomain(domain: String): String = {
    if (domain.startsWith(".")) null else {
    val arr = domain.split("\\.")
    val length = arr.length
    val tld = length match {
      case 1 => null
      case 2 => {
        val tld2 = arr(length - 2) + "." + arr(length - 1)
        if (isExist(TOP_LEVEL_DOMAIN_TWO, tld2)) tld2
        else {
          val tld1 = arr(length - 1)
          //println(tld1)
          if (isExist(TOP_LEVEL_DOMAIN_ONE, tld1)) tld1 else null
        }
      }
      case 3 => {
        val tld3 = arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
        if (isExist(TOP_LEVEL_DOMAIN_THREE, tld3)) tld3
        else {
          val tld2 = arr(length - 2) + "." + arr(length - 1)
          if (isExist(TOP_LEVEL_DOMAIN_TWO, tld2)) tld2
          else {
            val tld1 = arr(length - 1)
            if (isExist(TOP_LEVEL_DOMAIN_ONE, tld1)) tld1 else null
          }
        }
      }
      case 4 => {
        val tld4 = arr(length - 4) + "." + arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
        if (isExist(TOP_LEVEL_DOMAIN_FOUR, tld4)) tld4
        else {
          val tld3 = arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
          if (isExist(TOP_LEVEL_DOMAIN_THREE, tld3)) tld3
          else {
            val tld2 = arr(length - 2) + "." + arr(length - 1)
            if (isExist(TOP_LEVEL_DOMAIN_TWO, tld2)) tld2
            else {
              val tld1 = arr(length - 1)
              if (isExist(TOP_LEVEL_DOMAIN_ONE, tld1)) tld1 else null
            }
          }
        }
      }
      case _ => {
        val tld5 = arr(length - 5) + "." + arr(length - 4) + "." + arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
        if (isExist(TOP_LEVEL_DOMAIN_FIVE, tld5)) tld5 else {
          val tld4 = arr(length - 4) + "." + arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
          if (isExist(TOP_LEVEL_DOMAIN_FOUR, tld4)) tld4
          else {
            val tld3 = arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
            if (isExist(TOP_LEVEL_DOMAIN_THREE, tld3)) tld3
            else {
              val tld2 = arr(length - 2) + "." + arr(length - 1)
              if (isExist(TOP_LEVEL_DOMAIN_TWO, tld2)) tld2
              else {
                val tld1 = arr(length - 1)
                if (isExist(TOP_LEVEL_DOMAIN_ONE, tld1)) tld1 else null
              }
            }
          }
        }
      }
    }
    tld
    //    def f = (x: String) => domain.endsWith("." + x)
    //    val tld5 = TOP_LEVEL_DOMAIN_FIVE_SUB.find(f).getOrElse(null)
    //    val tld4 = if (tld5 == null) TOP_LEVEL_DOMAIN_FOUR_SUB.find(f).getOrElse(null) else tld5
    //    val tld3 = if (tld4 == null) TOP_LEVEL_DOMAIN_THREE_SUB.find(f).getOrElse(null) else tld4
    //    val tld2 = if (tld3 == null) TOP_LEVEL_DOMAIN_TWO_SUB.find(f).getOrElse(null) else tld3
    //    val tld1 = if (tld2 == null) TOP_LEVEL_DOMAIN_ONE_SUB.find(f).getOrElse(null) else tld2
    //    tld1
    }
  }
  
  private def getTopLevelDomainBk(domain: String): String = {
    if (domain.startsWith(".")) null else {
    val arr = domain.split("\\.")
    val length = arr.length
    val tld = length match {
      case 1 => null
      case 2 => {
        val tld2 = arr(length - 2) + "." + arr(length - 1)
        if (TOP_LEVEL_DOMAIN_TWO.contains(tld2)) tld2
        else {
          val tld1 = arr(length - 1)
          if (TOP_LEVEL_DOMAIN_ONE.contains(tld1)) tld1 else null
        }
      }
      case 3 => {
        val tld3 = arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
        if (TOP_LEVEL_DOMAIN_THREE.contains(tld3)) tld3
        else {
          val tld2 = arr(length - 2) + "." + arr(length - 1)
          if (TOP_LEVEL_DOMAIN_TWO.contains(tld2)) tld2
          else {
            val tld1 = arr(length - 1)
            if (TOP_LEVEL_DOMAIN_ONE.contains(tld1)) tld1 else null
          }
        }
      }
      case 4 => {
        val tld4 = arr(length - 4) + "." + arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
        if (TOP_LEVEL_DOMAIN_FOUR.contains(tld4)) tld4
        else {
          val tld3 = arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
          if (TOP_LEVEL_DOMAIN_THREE.contains(tld3)) tld3
          else {
            val tld2 = arr(length - 2) + "." + arr(length - 1)
            if (TOP_LEVEL_DOMAIN_TWO.contains(tld2)) tld2
            else {
              val tld1 = arr(length - 1)
              if (TOP_LEVEL_DOMAIN_ONE.contains(tld1)) tld1 else null
            }
          }
        }
      }
      case _ => {
        val tld5 = arr(length - 5) + "." + arr(length - 4) + "." + arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
        if (TOP_LEVEL_DOMAIN_FIVE.contains(tld5)) tld5 else {
          val tld4 = arr(length - 4) + "." + arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
          if (TOP_LEVEL_DOMAIN_FOUR.contains(tld4)) tld4
          else {
            val tld3 = arr(length - 3) + "." + arr(length - 2) + "." + arr(length - 1)
            if (TOP_LEVEL_DOMAIN_THREE.contains(tld3)) tld3
            else {
              val tld2 = arr(length - 2) + "." + arr(length - 1)
              if (TOP_LEVEL_DOMAIN_TWO.contains(tld2)) tld2
              else {
                val tld1 = arr(length - 1)
                if (TOP_LEVEL_DOMAIN_ONE.contains(tld1)) tld1 else null
              }
            }
          }
        }
      }
    }
    tld
    //    def f = (x: String) => domain.endsWith("." + x)
    //    val tld5 = TOP_LEVEL_DOMAIN_FIVE_SUB.find(f).getOrElse(null)
    //    val tld4 = if (tld5 == null) TOP_LEVEL_DOMAIN_FOUR_SUB.find(f).getOrElse(null) else tld5
    //    val tld3 = if (tld4 == null) TOP_LEVEL_DOMAIN_THREE_SUB.find(f).getOrElse(null) else tld4
    //    val tld2 = if (tld3 == null) TOP_LEVEL_DOMAIN_TWO_SUB.find(f).getOrElse(null) else tld3
    //    val tld1 = if (tld2 == null) TOP_LEVEL_DOMAIN_ONE_SUB.find(f).getOrElse(null) else tld2
    //    tld1
    }
  }

  private def isExist(topLevel: HashSet[String], tld: String): Boolean = {
    val index = tld.indexOf(".")
    val tld2 = if (index > 0) "*" + tld.substring(index) else tld
    topLevel.contains(tld) || topLevel.contains(tld2)
  }

  private def getTopLevelDomain(): List[String] = {
    (getEffectiveTLD() ++ getSuffixTLD() ++ getOpenNicTLD()).distinct.map(x => x.trim())
  }
  
  private def getEffectiveTLD(): List[String] = {
    FileUtil.readResource("effective_tld_names.dat")
      .filter(x => !x.startsWith("#"))
      .filter(x => !x.startsWith("//"))
      .filter(x => StringUtil.isNotNullAndEmpty(x))
  }
  
  private def getSuffixTLD(): List[String] = {
    FileUtil.readResource("suffix-tld.csv")
      .filter(x => !x.startsWith("#"))
      .map(x => x.split("\t"))
      .map(x => x(1).replace("\"", ""))
      .filter(x => StringUtil.isNotNullAndEmpty(x))
  }
  
  private def getOpenNicTLD(): List[String] = {
    FileUtil.readResource("opennic-tld.csv")
      .filter(x => !x.startsWith("#"))
      .filter(x => StringUtil.isNotNullAndEmpty(x))
  }

  def isValid(domain: String): Boolean = {
    /**
     * googleapis.com: is a private toplevel domain
     * => invalid domain
     * 	TLD = googleapis.com
     * 	SLD = null
     * 	FQDN = googleapis.com
     * 
     * com: is top level domain
     * => invalid domain
     *  TLD = com
     *  SLD = null
     *  FQDN = com
     */
    
    /**
     * Currently, this function will return true with the domain only has top level domain such as: googleapis.com, com
     * This is Ok for analytic dns log.
     * Fix this code later.
     */
    
    val rs = (split(domain).tld != null) && (split(domain).second != null)
    //println(domain + ":" + rs)
    rs
  }

  def main(args: Array[String]) {
//    val url = "http://ec2-52-87-183-97.compute-1.amazonaws.com"
    val url = "potzywlol"
    val domainSplit = DomainUtil.extract(url)
    println(domainSplit)
    println(domainSplit.second)
//    println(TOP_LEVEL_DOMAIN_ONE.contains("bit"))
    //TOP_LEVEL_DOMAIN_ONE.foreach(println)
    //println(TOP_LEVEL_DOMAIN_ONE.contains("bit"))
//    val tld = getTopLevelDomain()
//    tld.foreach(println)
//    println(tld.size)
  }
}