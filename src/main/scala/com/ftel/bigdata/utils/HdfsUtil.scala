package com.ftel.bigdata.utils

import org.apache.hadoop.conf.Configuration
import java.net.URI
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import java.io.InputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.collection.immutable.HashMap

import org.apache.hadoop.fs.FileUtil._
import java.io.OutputStream
import java.io.BufferedWriter
import java.io.OutputStreamWriter

object HdfsUtil {
  
  def getFileSystem(host: String, port: Int): FileSystem = {
    val uri = s"hdfs://$host:$port";
    val configuration = new Configuration();
    configuration.set("fs.defaultFS", uri)
    configuration.set("fs.hdfs.impl", classOf[org.apache.hadoop.hdfs.DistributedFileSystem].getName)
    configuration.set("fs.file.impl", classOf[org.apache.hadoop.fs.LocalFileSystem].getName)
    FileSystem.get(configuration);
  }

  def isExist(host: String, port: Int, path: String): Boolean = {
    val fs = getFileSystem(host, port)
    try {
      fs.exists(new Path(path))
    } finally {
      fs.close()
    }
  }

  def isExist(fs: FileSystem, path: String): Boolean = {
    fs.exists(new Path(path))
  }

  def ls(host: String, port: Int, dir: String): List[String] = {
    val fs = getFileSystem(host, port)
    try {
      fs.listStatus(new Path(dir)).map { x => x.getPath.toUri.getPath }.toList
    } finally fs.close()
  }

  /**
   * List only files in directory
   */
  def ls(host: String, port: Int, dir: String, recursive: Boolean): List[String] = {
    val fs = getFileSystem(host, port)
    try {
      val files = fs.listFiles(new Path(dir), recursive)
      def convert(acc: List[String]):List[String] = {
        if (files.hasNext()) convert(files.next().getPath.toUri().getPath :: acc)
        else acc
      }
      convert(List())
    } finally fs.close()
  }

  def ls(fs: FileSystem, dir: String, recursive: Boolean): List[String] = {
      val files = fs.listFiles(new Path(dir), recursive)
      def convert(acc: List[String]):List[String] = {
        if (files.hasNext()) convert(files.next().getPath.toUri().getPath :: acc)
        else acc
      }
      convert(List())
  }

  def mkdir(host: String, port: Int, dir: String): Unit = {
    val fs = getFileSystem(host, port)
    try {
      fs.mkdirs(new Path(dir))
    } finally fs.close()
  }

  def mkdir(fs: FileSystem, dir: String): Unit = {
    fs.mkdirs(new Path(dir))
  }
  
  def read(host: String, port: Int, dir: String): List[String] = {
    val fs = getFileSystem(host, port)
    try {
      val files = fs.listFiles(new Path(dir), false)
      
      def readBuff(br: BufferedReader, line: String, acc: List[String]): List[String] = {
        if (line == null) acc
        else {
          readBuff(br, br.readLine(), acc :+ line)
        }
      }
      
      def readFile(acc: List[String]): List[String] = {
        if (files.hasNext()) {
          val in: InputStream = fs.open(files.next().getPath)
          val br: BufferedReader = new BufferedReader(new InputStreamReader(in))
          try {
            readFile(readBuff(br, br.readLine(), List()) ::: acc)
          } finally br.close()
        } else acc
      }
      readFile(List())
    } finally fs.close()
  }
  
  def write(host: String, port: Int, file: String, contents: Map[String, String]): Unit = {
        val fs = getFileSystem(host, port)
        val os: OutputStream = fs.create(new Path(file), true)
        val bw: BufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"))
        contents.foreach(x => bw.write(s"(${x._1},${x._2})\n") )
        bw.close();
    }
  
  def mv(host: String, port: Int, src: String, dst: String): Unit = {
    val fs = getFileSystem(host, port)
    try {
      fs.rename(new Path(src), new Path(dst))
    } catch {
      case e: Exception => println(e)
    } finally fs.close()
  }
  
  def mv(fs: FileSystem, src: String, dst: String): Unit = {
    try {
      fs.rename(new Path(src), new Path(dst))
    } catch {
      case e: Exception => println(e)
    }
  }
  
  def cp(host: String, port: Int, src: String, dst: String): Unit = {
    val fs = getFileSystem(host, port)
    try {
      val uri = s"hdfs://$host:$port"
      copy(fs, new Path(src), fs, new Path(dst), false, new Configuration()) 
    } finally fs.close()
  }
  
  def rm(host: String, port: Int, file: String): Unit = {
    val fs = getFileSystem(host, port)
    try {
      val uri = s"hdfs://$host:$port"
      fs.delete(new Path(file), true)
    } finally fs.close()
  }

  
  
//  def main(args: Array[String]) {
//    val host = args(0)
//    val port = args(1).toInt
//    val topicName = args(2)
//    //mkdir("172.17.0.2",9000,"/test/a")
//    //mkdir("172.17.0.2",9000,"/test/b")
//    //mkdir("172.17.0.2",9000,"/test/c")
//    //println(isExist("172.17.0.2",9000,"/test"))
//    //println(ls("172.17.0.2",9000,"/test/a/realtime-vs-ml.txt/part-00001", false))
//    val hdfs: HostPort = new HostPort(host, port)
//    val topicType: TopicParameter = new TopicParameter(topicName)
//    //val res = getPartitionOffsetKafka(hdfs, topicType)
//    //res.toArray().foreach(println)
//    //println(ls(host,port,"/"))
//    //println(ls(host,port,"/data"))
//    //println(ls(host,port,"/data/kafka/"))
//  }
}