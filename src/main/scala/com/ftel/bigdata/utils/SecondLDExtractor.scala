package com.ftel.bigdata.utils

import java.util.regex.Pattern
import java.util.ArrayList
import java.io.InputStreamReader
import java.io.BufferedReader
import java.util.Collections
import scala.collection.immutable.HashSet



object SecondLDExtractor {
  //val sb = new StringBuilder()
  //var pattern: Pattern

  val TOP_LEVEL = HashSet() ++ FileUtil.readResource("/com/ftel/bigdata/utils/effective_tld_names.dat").filter(x => !x.startsWith("//") && !x.isEmpty())
  private val pattern = {
    println("------")
    //try {
//      val terms = new ArrayList[String]();
//
//      val br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("effective_tld_names.dat")));
//      String s = null;
//      while ((s = br.readLine()) != null) {
//        s = s.trim();
//        if (s.length() == 0 || s.startsWith("//") || s.startsWith("!")) continue;
//        terms.add(s);
//      }
      val lines = TOP_LEVEL.toList
      val linesSort = lines.sortWith((x,y) => x.length() > y.length()).toArray
      val s = linesSort.map(x => x.replace(".", "\\."))
               .map(x => "\\." + x )
               .map(x => if (x.startsWith("*")) x.replace("*", ".+") else x)
               .mkString("|")
      val s2 = "[^.]+?(" + (if (s.length() > 0) s.substring(0, s.length()-1) else s) + ")$"
      Pattern.compile(s2);
      //Collections.sort(lines, new StringLengthComparator());
      //for(String t: terms) add(t);
      //compile();
      //br.close();
    //} catch (IOException e) {
    //  throw new IllegalStateException(e);
    //}
  }

//  protected void add(String s) {
//    s = s.replace(".", "\\.");
//    s = "\\." + s;
//    if (s.startsWith("*")) {
//      s = s.replace("*", ".+");
//      sb.append(s).append("|");
//    } else {
//      sb.append(s).append("|");
//    }
//  }

//  public void compile() {
//    if (sb.length() > 0) sb.deleteCharAt(sb.length() – 1);
//    sb.insert(0, "[^.]+?(");
//    sb.append(")$");
//    pattern = Pattern.compile(sb.toString());
//    sb = null;
//  }

  def extract(host: String): DomainSplit = {
    val m = pattern.matcher(host)
    if (m.find()) {
      val secondTemp = try {m.group(0)}catch{case e: Exception => null}
      val tld = if (TOP_LEVEL.contains(secondTemp)) secondTemp 
      else try {m.group(1).substring(1)}catch{case e: Exception => null}
      val second = if (TOP_LEVEL.contains(secondTemp)) null else secondTemp
      DomainSplit(host, second, tld)
    } else DomainSplit(host, null, null)
  }

//  def extractTLD(host: String): String = {
//    val m = pattern.matcher(host);
//    if (m.find()) {
//      m.group(1);
//    } else null
////    return null;
//  }

//  public static class StringLengthComparator implements Comparator<String> {
//    public int compare(String s1, String s2) {
//      if (s1.length() > s2.length()) return -1;
//      if (s1.length() < s2.length()) return 1;
//      return 0;
//    }
//  }
}