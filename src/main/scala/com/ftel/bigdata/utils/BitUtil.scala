package com.ftel.bigdata.utils

object BitUtil {
  
  def binary2decimal = (s:String) => Integer.parseInt(s, 2)
  //def decimal2binary = (i: Int) => i.toBinaryString
  def decimal2binary(i: Long, digits: Int = 32) = String.format("%" + digits + "s", i.toBinaryString).replace(' ', '0')
  
  /**
   * Create a decimal with turn on a bit at position i-th
   * 
   * @Paramter: i - bit at position i-th
   * @Return: decimal number
   */
  def decimalWithTurnOn(i: Int = 0) = 1 << i //Math.pow(2, i).toLong//decimal2binary(Math.pow(2, i).toLong, 32)
  
  def main(args: Array[String]) {
    val a = 1
    val b = 2
    val c = a | b
    println(c)
    //println(decimal2binary(4))
    //println(Integer.parseInt("0110", 2))
//    println(s)
    
    for (i <- 0 until 32) println(i -> decimalWithTurnOn(i))
//    println(create32Bit(1))
//    println(create32Bit())
    
    //268,435,456
    println(decimalWithTurnOn(27) | decimalWithTurnOn(27))
  }
}