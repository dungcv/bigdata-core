package com.ftel.bigdata.utils

import scala.collection.immutable.HashSet

object CountryUtil {
  private val DOMAIN_CODE_TO_COUNTRY = getCountryCodeTLD()
  private val DOMAIN_COUNTRY_TO_CODE = DOMAIN_CODE_TO_COUNTRY.map(x => x._2 -> x._1)
  private val LAND_MAP = getLanguage()
  val DOMAIN_COUNTRY_CODE = HashSet() ++ DOMAIN_CODE_TO_COUNTRY.map(x => x._1)
  
  private val DMOZ_LANG_MAP = getLanguageDmoz()
  
  private def getCountryCodeTLD(): Map[String, String] = {
    FileUtil.readResource("ccTLD.csv")
            .filter(x => !x.startsWith("#"))
            .map(x => x.split(","))
            .map(x => x(0).toLowerCase() -> x(1))
            .toMap
  }
  
  private def getCountryCode(): Map[String, String] = {
    FileUtil.readResource("countryCode.csv")
            .map(x => x.split("\t"))
            .map(x => x(0).toLowerCase() -> x(1))
            .toMap
  }

  private def getLanguage(): Map[String, String] = {
    FileUtil.readResource("land2country.csv")
            .filter(x => !x.startsWith("#"))
            .map(x => x.split(";"))
            .map(x => x.slice(4, 10) -> x(1).toLowerCase())
            .flatMap(x => x._1.map(y => y -> x._2))
            .toMap
  }
  
  private def getLanguageDmoz(): Map[String, String] = {
    FileUtil.readResource("language2code_dmoz.csv")
            .filter(x => !x.startsWith("#"))
            .map(x => x.split("\t"))
            .map(x => x(0) -> (if (x.length == 2) x(1).toLowerCase() else null))
            .toMap
  }

  def getCountry(code: String): String = DOMAIN_CODE_TO_COUNTRY.getOrElse(code, null)
  def getCode(country: String): String = DOMAIN_COUNTRY_TO_CODE.getOrElse(country, null)
  def getCodeFromLanguage(land: String): String = LAND_MAP.getOrElse(land, null)
  def getCodeFromLanguageDmoz(lang: String): String = DMOZ_LANG_MAP.getOrElse(lang, null)
  
}