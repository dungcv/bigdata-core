package com.ftel.bigdata.utils

import scala.util.Try

/**
 * Utilities for String.
 *
 * @author dungcv
 * @since 0.0.1
 *
 */
object StringUtil {
  /**
   * Create a empty String.
   * @return empty
   */
  val EMPTY = ""

  /**
   * Check string is null or empty.
   *
   * @param string - string for check
   * @return true if null or empty, false if others
   */
  def isNullOrEmpty(s: String): Boolean = {
    if (s == null) true else s.isEmpty()
  }

  /**
   * Check string is not null and empty.
   *
   * @param string - string for check
   * @return false if null or empty, true if others
   */
  def isNotNullAndEmpty(s: String): Boolean = {
    !isNullOrEmpty(s)
  }

  /**
   * Get subString between 2 pattern and return substring occur first.
   *
   * @param origin        - origin string
   * @param beginString   - pattern begin
   * @param nextString    - pattern next, this string will behind beginString.
   *          The String is the same with nextString that precede beginString will be ignored.
   * Exp: abcabeffbcdfabc => subString(ab, bc) = eff.
   * @return subString between 2 pattern, if don't exist string between 2 pattern, return empty.
   */
  def subString(origin: String, beginString: String, nextString: String): String = {
    val index = origin.indexOf(beginString)
    val beginIndex = if (index > 0) index + beginString.length() else -1
    val endIndex = beginIndex + origin.substring(beginIndex).indexOf(nextString)
    if (endIndex > beginIndex && beginIndex > beginString.length() && endIndex < origin.length()) {
      return origin.substring(beginIndex, endIndex);
    } else EMPTY
  }

  /**
   * Get subString from 1 pattern  to end string.
   *
   * @param origin        - origin string
   * @param beginString   - pattern begin
   * @return subString from index of pattern to end string, if don't exist => return empty.
   */
  def subString(origin: String, beginString: String): String = {
    val index = origin.indexOf(beginString)
    if (index <= 0) EMPTY
    else {
      val beginIndex = index + beginString.length()
      if (beginIndex < origin.length()) {
        return origin.substring(beginIndex);
      } else EMPTY
    }
  }

  /**
   * Add more character before number.
   *
   * @param number                - number
   * @param numberOfCharacter     - max number of character after add more.
   *   Exp: numberOfCharacter = 2, number = 1, character=0  => 01
   *        numberOfCharacter = 2, number = 12, character=0 => 12
   * @param character             - character
   * @return new string with character
   */
  def addSpecialCharacterBeforeNumber(number: Long, numberOfCharacter: Int, character: String): String = {
    ("%" + character + numberOfCharacter + "d").format(number)
  }

  /**
   * Add more space character before number.
   *
   * @param number    - number
   * @param spaces    - max number of character after add more space
   * @return new string with spaces
   */
  def addSpacesBeforeNumber(number: Long, spaces: Int): String = {
    addSpecialCharacterBeforeNumber(number, spaces, " ")
  }

  /**
   * Check s is number.
   *
   * @param str - String
   * @return true is number format, false is text
   */
  def isNumeric(s: String): Boolean = {
    Try(s.toDouble).isSuccess
  }

  /**
   * This method returns a copy of the string, with leading and trailing
   * "special string" omitted.
   *
   * @param origin    - origin string
   * @param omitted   - string which will be removed head and tail
   * @return new String with omitted trimed
   */
  def trim(origin: String, omitted: String): String = {
//    if (isNullOrEmpty(origin)) origin
//    else {
//      if (origin.startsWith(omitted)) {
//        origin.substring(omitted.length());
//      } else {
//        origin.substring(0, origin.length() - omitted.length());
//      }
//    }
    // Update 2018-02-13
    if (isNullOrEmpty(origin)) origin
    else {
      if (origin.startsWith(omitted) && origin.endsWith(omitted)) {
        origin.substring(omitted.length(), origin.length() - omitted.length())
      } else if (origin.startsWith(omitted)) {
        origin.substring(omitted.length())
      } else if (origin.endsWith(omitted)) {
        origin.substring(0, origin.length() - omitted.length())
      } else {
        origin
      }
    }
  }

  /**
   * Remove white space head and tail of string in safe mode.
   *
   * @param origin    - origin string
   * @return new String with omitted trimed
   */
  def trim(origin: String): String = if (origin == null) null else origin.trim()

  /**
   * Remove all white space in string.
   */
  def removeAllWhiteSpace(s: String): String = {
    s.replaceAll("\\s", "")
  }

  /**
   * Replace 2 or more space to one space in string.
   */
  def replaceAllWhiteSpace(s: String): String = {
    //println(s.replaceAll("\\s{2,}", " ").trim())
    s.replaceAll("\\s{2,}", " ").trim()
  }
}