package com.ftel.bigdata.utils

import com.optimaize.langdetect.LanguageDetectorBuilder
import com.optimaize.langdetect.profiles.LanguageProfileReader
import com.optimaize.langdetect.ngram.NgramExtractors
import com.optimaize.langdetect.text.CommonTextObjectFactories

object LangDetectUtil {
  private val LANGUAGE_PROFILES = new LanguageProfileReader().readAllBuiltIn();
  private val LANGUAGE_DETECTOR = LanguageDetectorBuilder
    .create(NgramExtractors.standard())
    .withProfiles(LANGUAGE_PROFILES)
    .build();
  private val TEXT_OBJECT_FACTORY = CommonTextObjectFactories.forDetectingOnLargeText();
  val LANGUAGE_UNKNOW = "N/A"

  /**
   * NOTE: Text must be more than 200 characters, if less more 200, It is not guarantee correct
   */
  def getLanguageCode(text: String): String = {
    val textObject = TEXT_OBJECT_FACTORY.forText(text);
    val lang = LANGUAGE_DETECTOR.detect(textObject);
    if (lang.isPresent()) lang.get.getLanguage else LANGUAGE_UNKNOW
  }

  def main(args: Array[String]) {
    println(getLanguageCode("小松（中国）投资有限公司专业生产小松挖掘机、小松装载机、小松矿用车等工程机械"))
    println(getLanguageCode("Công ty lọc hóa dầu Bình Sơn"))
    println(getLanguageCode("Az Externet Magyarország legnagyobb alternatív távközlési szolgáltatója. Lakossági és üzleti ügyfeleknek egyaránt kínál internet"))
    println(getLanguageCode("View more than 20 million economic indicators for 196 countries. Get free indicators, Historical Data, Charts, News and Forecasts for 196 countries."))
  }
}