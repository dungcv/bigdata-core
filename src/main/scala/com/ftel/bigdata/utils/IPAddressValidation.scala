package com.ftel.bigdata.utils

object IPAddressValidation {
  private[this] val IP6_PATTERN = "([a-f0-9]{1,4}:){7}\\b[0-9a-f]{1,4}\\b"
  private[this] val IP4_PATTERN =
    "\\b(((2[0-5][0-5])|(1[0-9][0-9])|(\\b[1-9][0-9]\\b)|(\\b\\d\\b))\\.){3}((2[0-5][0-5])|(1[0-9][0-9])|(\\b[1-9][0-9]\\b)|(\\b\\d\\b))\\b"

//  val lines = Source.stdin.getLines().drop(1)
//  val validation = lines.map(ipValidation)
//  println(validation.mkString("\n"))

  def ipValidation(address: String): String = {
    if (address.matches(IP6_PATTERN))
      "IPv6"
    else if (address.matches(IP4_PATTERN))
      "IPv4"
    else
      "Neither"
  }
  
  def isIp4Remove(address: String): Boolean = {
    val res = ipValidation(address)
    if ("IPv4" == res) true else false
  }

  def isIP4Valid(ip: String): Boolean = {
    if (ip == null || ip.isEmpty()) {
      false
    } else {
      val parts = ip.split("\\.");
      if (parts.length != 4) {
        false;
      } else {
        if (parts.find {
            x => NumberUtil.toInt(x,-1) < 0 || NumberUtil.toInt(x,-1) > 255
        }.isEmpty) {
          if (ip.endsWith(".")) {
            false
          } else true
        } else false
      }
    }
  }
  
  def main(args: Array[String]) {
    val ip6 = "2405:4800:58b4:c4c4:4823:8099:1cfc:8e6e"
    val ip4 = "27.3.228.3"
    val noIP41 = "27.256.228.3"
    val noIP42 = "27.abc.228.3"
    val ip42 = "10.38.147.144"
    
    println(isIP4Valid(ip4))
    println(isIP4Valid(ip42))
    println(isIP4Valid(noIP41))
    println(isIP4Valid(noIP42))
    println(isIP4Valid(ip6))
  }
}