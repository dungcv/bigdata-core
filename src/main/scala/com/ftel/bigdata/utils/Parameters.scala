package com.ftel.bigdata.utils

object Parameters {
  val TAB = "\t"
  
  val ES_5_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
  
  val SPARK_READ_DIR_RECURSIVE = "mapreduce.input.fileinputformat.input.dir.recursive"
}