package com.ftel.bigdata.utils

/**
 * Class define some function statistic and probability
 */
object StatisticUtil {
  /**
   * Calculate variance for Array
   */
  def variance(xs: Iterable[Double]): Double = {
    if (xs.size > 1) {
      val m = mean(xs)
      mean(xs.map(x => Math.pow(x - m, 2))) // <==> val sum = xs.map(x => Math.pow(x-m, 2)).sum; return (sum / xs.size)
    } else 0
  }

  /**
   * Calculate average for Array
   */
  def mean(xs: Iterable[Double]): Double = {
    xs.sum / xs.size
  }
}