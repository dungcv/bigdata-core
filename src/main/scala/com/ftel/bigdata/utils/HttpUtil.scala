package com.ftel.bigdata.utils

//import java.net.InetSocketAddress
//import java.net.URL
//import java.net.Proxy
//import java.io.DataOutputStream
//import java.io.BufferedReader
//import java.io.InputStreamReader
//import java.io.IOException
import scalaj.http.{Http, Token}
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import java.time.Duration.DurationUnits
import java.util.concurrent.TimeUnit

object HttpUtil {

  /**
   * Get content form url
   */
  def getContent(url: String): String = {
    //getContentWithProxy(url, null)
    Http(url).asString.body
  }

  /**
   * Get content from url with proxy
   */
  def getContent(url: String, proxyHost: String, proxyPort: Int): String = {
    //val proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort))
    //getContentWithProxy(url, proxy)
    val response = Http(url).proxy(proxyHost, proxyPort).asString
    response.body
  }

  def download(url: String, out: String) {
    val response = Http(url).asBytes.body
    FileUtil.write(out, response)
  }
  
  def download(url: String, out: String, proxyHost: String, proxyPort: Int) {
    val response = Http(url).proxy(proxyHost, proxyPort).asBytes.body
     FileUtil.write(out, response)
  }
  
  /**
   * Get content from url
   */
//  private def getContentWithProxy(url: String, proxy: Proxy): String = {
//    try {
//      
//      val connection = if (proxy != null) new URL(url).openConnection(proxy) else new URL(url).openConnection()
//      //connection.setRequestMethod("POST");
//      connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//      connection.setRequestProperty("Content-Length", Integer.toString(url.getBytes().length));
//      connection.setRequestProperty("Content-Language", "en-US");
//      connection.setUseCaches(false);
//      connection.setDoOutput(true);
//
//      //Send request
//      val wr = new DataOutputStream(connection.getOutputStream());
//      wr.writeBytes(url);
//      wr.close();
//
//      //Get Response  
//      val is = connection.getInputStream();
//      val rd = new BufferedReader(new InputStreamReader(is));
//      def getResponse(acc: List[String]): List[String] = {
//        if (rd.ready()) getResponse(rd.readLine() :: acc)
//        else acc
//      }
//      val response = getResponse(List())
//      rd.close();
//      response.mkString("\r")
//    } catch {
//      case e: IOException => println(e); "Exception => " + e.getMessage
//    }
//  }
}