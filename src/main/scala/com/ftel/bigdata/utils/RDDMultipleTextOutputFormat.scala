package com.ftel.bigdata.utils

import org.apache.hadoop.io.NullWritable
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat
import scala.reflect.ClassTag
import org.apache.spark.rdd.RDD
import org.apache.spark.HashPartitioner

class RDDMultipleTextOutputFormat extends MultipleTextOutputFormat[Any, Any] {
  override def generateActualKey(key: Any, value: Any): Any =
    NullWritable.get()

  override def generateFileNameForKeyValue(key: Any, value: Any, name: String): String =
    key.asInstanceOf[String]
}

object RDDMultipleTextOutputFormat {
  def save[T: ClassTag](rdd: RDD[T], f: T => String, output: String) {
    rdd.map(x => f(x) -> x)
      //.partitionBy(new HashPartitioner(10))
      .saveAsHadoopFile(output, classOf[String], classOf[String], classOf[RDDMultipleTextOutputFormat])
  }

  def save[T: ClassTag](rdd: RDD[T], f: T => String, output: String, partition: Int) {
    rdd.map(x => f(x) -> x)
      .partitionBy(new HashPartitioner(partition))
      .saveAsHadoopFile(output, classOf[String], classOf[String], classOf[RDDMultipleTextOutputFormat])
  }

  def saveCompress[T: ClassTag](rdd: RDD[T], f: T => String, output: String, partition: Int) {
    rdd.map(x => f(x) -> x)
      .partitionBy(new HashPartitioner(partition))
      .saveAsHadoopFile(output,
          classOf[String], classOf[String],
          classOf[RDDMultipleTextOutputFormat],
          classOf[org.apache.hadoop.io.compress.GzipCodec])
  }
}