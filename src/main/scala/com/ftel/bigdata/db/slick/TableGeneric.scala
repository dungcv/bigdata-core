package com.ftel.bigdata.db.slick

import slick.lifted.Rep
import slick.driver.PostgresDriver.api.Table
import slick.driver.PostgresDriver.api.Tag
import scala.reflect.ClassTag

abstract class TableGeneric[T: ClassTag](tag: Tag, tableName: String) extends Table[T](tag, tableName) {
  def id: Rep[Int]
  def name: Rep[String]
}