package com.ftel.bigdata.db.slick

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.reflect.ClassTag

import org.apache.spark.rdd.RDD

//import com.ftel.bigdata.conf.Configure
//import com.ftel.bigdata.model.StatusTable
//import com.ftel.bigdata.model.TableGeneric

import slick.driver.PostgresDriver.api.Database
import slick.driver.PostgresDriver.api.columnExtensionMethods
import slick.driver.PostgresDriver.api.intColumnType
import slick.driver.PostgresDriver.api.queryInsertActionExtensionMethods
import slick.driver.PostgresDriver.api.streamableQueryActionExtensionMethods
import slick.driver.PostgresDriver.api.stringColumnType
import slick.lifted.Query
import slick.lifted.TableQuery
import com.typesafe.config.Config

/**
 * https://stackoverflow.com/questions/20090228/how-to-define-generic-type-in-scala
 */
class PostgresSlick(config: Config, key: String) extends SlickTrait {
  
  val db: Database = Database.forConfig(key, config)
  def close() = db.close()

  def insert[T: ClassTag, B: ClassTag](rows: List[T], groupSize: Int, table: TableQuery[_ <: TableGeneric[B]], f: T => B): Map[String, Int] = {
    try {
      rows.grouped(groupSize).map(x => insert[T, B](x, table, f)).flatten.toMap
    } catch {
      case e: Exception => println(e); Map[String, Int]()
    }
  }

  private def insert[T: ClassTag, B: ClassTag](rows: List[T], table: TableQuery[_ <: TableGeneric[B]], f: T => B): Map[String, Int] = {
    try {
      val action = ((table returning table.map(x => x.name -> x.id)) ++= rows.map(f))
      val setupFuture = db.run(action)
      val result = Await.result(setupFuture, Duration.Inf)
      result.toMap
    } catch {
      case e: Exception => println(e); Map[String, Int]()
    }
  }

  def insertOrUpdate[T: ClassTag, B: ClassTag](rows: List[T], groupSize: Int, table: TableQuery[_ <: TableGeneric[B]], f: T => B): Map[String, Int] = {
    try {
      rows.grouped(groupSize).map(x => insertOrUpdate[T, B](x, table, f)).flatten.toMap
    } catch {
      case e: Exception => println(e); Map[String, Int]()
    }
  }

  private def insertOrUpdate[T: ClassTag, B: ClassTag](rows: List[T], table: TableQuery[_ <: TableGeneric[B]], f: T => B): Map[String, Int] = {
    try {
      val result = rows.map(x => {
        val action = ((table returning table.map(x => x.name -> x.id)) insertOrUpdate(f(x)))
        val setupFuture = db.run(action)
        val pair = Await.result(setupFuture, Duration.Inf)
        pair.getOrElse(null)
      }).filter(x => x!= null).toMap
//      val action =  => ((table returning table.map(x => x.name -> x.id)) insertOrUpdate(f(x))))
//      val setupFuture = db.run(action)
//      val result = Await.result(setupFuture, Duration.Inf)
//      result.toMap
      result
    } catch {
      case e: Exception => println(e); Map[String, Int]()
    }
  }
  
//  def insertRDD[T: ClassTag, B: ClassTag](rows: RDD[T], groupSize: Int, table: TableQuery[_ <: TableGeneric[B]], f: T => B): Map[String, Int] = {
//    val preservesPartitioning = false
//    val result = rows.mapPartitions(
//      x => insert[T, B](db, x.toList, groupSize, table, f).toIterator,
//      preservesPartitioning)
//      .collect()
//    result.toMap
//  }

  /**
   * GEt Ids by name list
   * Return: Array[(sTring, Int)]
   */
  def getIdByNames[T <: TableGeneric[_]: ClassTag](table: TableQuery[T], names: Array[String]): Map[String, Int] = {
    val query: Query[_, (String, Int), Seq] = table.filter(x => x.name.inSet(names)).map(x => x.name -> x.id)
    val setupFuture = db.run(query.result)
    val result = Await.result(setupFuture, Duration.Inf)
    result.toMap
  }

  /**
   * GEt Ids by name list
   * Return: Array[(sTring, Int)]
   */
  def getNameAndId[T <: TableGeneric[_]: ClassTag](table: TableQuery[T]): Map[String, Int] = {
    val query: Query[_, (String, Int), Seq] = table.map(x => x.name -> x.id)
    val setupFuture = db.run(query.result)
    val result = Await.result(setupFuture, Duration.Inf)
    result.toMap
  }
//
//  def main(args: Array[String]) {
//    val table: TableQuery[StatusTable] = TableQuery[StatusTable]
//    val res = getIdByNames[StatusTable](null, table, Array("ok", "hold"))
//    res.foreach(println)
//    println(res)
//    
//    //PostgresSlick.pipeline(null, null)
//  }
}