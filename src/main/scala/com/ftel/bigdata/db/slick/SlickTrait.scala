package com.ftel.bigdata.db.slick

import com.typesafe.config.Config
import slick.lifted.TableQuery
import scala.reflect.ClassTag

trait SlickTrait {
  def insert[T: ClassTag, B: ClassTag](rows: List[T], groupSize: Int, table: TableQuery[_ <: TableGeneric[B]], f: T => B): Map[String, Int]
  def insertOrUpdate[T: ClassTag, B: ClassTag](rows: List[T], groupSize: Int, table: TableQuery[_ <: TableGeneric[B]], f: T => B): Map[String, Int]
  def getIdByNames[T <: TableGeneric[_]: ClassTag](table: TableQuery[T], names: Array[String]): Map[String, Int]
  def getNameAndId[T <: TableGeneric[_]: ClassTag](table: TableQuery[T]): Map[String, Int]
}