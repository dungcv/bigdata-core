package com.ftel.bigdata.spark.es

import org.apache.spark.rdd.RDD
import org.elasticsearch.spark.rdd.EsSpark
import org.apache.spark.SparkConf
import org.elasticsearch.spark.sql.sparkDataFrameFunctions
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row

/**
 * Elasticsearch connection for Spark
 */
class EsConnection(esHost: String, esPort: Int, esIndex: String, esType: String) {
  def this(esHost: String, esPort: Int) = this(esHost, esPort, "", "")
  def configure(sparkConf: SparkConf) {
    /**
     *  Required settings
     */
    //sparkConf.set("es.resource", esIndex + "/" + esType)
    sparkConf.set("es.nodes", esHost + ":" + esPort) // List IP/Hostname/host:port
    //sparkConf.set("es.port", port)  // apply for host in es.nodes that do not have any port specified
    // For ES version 5.x, Using 'create' op will error if you don't set id when create
    // To automatic ID generate, using 'index' op
    sparkConf.set("es.write.operation", "index")
    sparkConf.set("es.batch.size.bytes", "10mb")
    sparkConf.set("es.batch.size.entries", Integer.toString(1000)) // default 1000
    sparkConf.set("es.batch.write.refresh", "true")
  }

  def save(rdd: RDD[_]) {
    save(rdd, esIndex, esType, null)
  }

  def save(rdd: RDD[_], index: String, _type: String) {
    save(rdd, index, _type, null)
  }

  def save(rdd: RDD[_], index: String, _type: String, idField: String) {
    // EsSpark.saveToEs(rdd, "spark/docs", Map("es.mapping.id" -> "id"))
    if (idField == null) EsSpark.saveToEs(rdd, s"${index}/${_type}")
    else EsSpark.saveToEs(rdd, s"${index}/${_type}", Map("es.mapping.id" -> idField))
  }
  
  def save(df: Dataset[Row], esIndex: String, esType: String, idField: String) {
    if (idField == null) df.saveToEs(s"${esIndex}/${esType}") 
    else df.saveToEs(s"${esIndex}/${esType}", Map("es.mapping.id" -> idField))
  }
}