package com.ftel.bigdata.whois

import com.ftel.bigdata.utils.Parameters.TAB
import com.ftel.bigdata.utils.DateTimeUtil
import com.ftel.bigdata.utils.StringUtil.isNotNullAndEmpty
import com.ftel.bigdata.utils.StringUtil
import org.apache.commons.net.whois.WhoisClient
import java.net.SocketException
import java.io.IOException
import scalaj.http.Http

/**
 * Domain name: String
 * - Registrar: String
 * - Whois server: String
 * - Referral URL: String
 * - Name server: Array[String]
 * - Status: Enum
 * + OK
 * + Client transfer prohibited
 * + hold
 * + redemption
 * - Creation date: DateTime
 * - Updated date: DateTime
 * - Expiration date: DateTime
 * - Lable: enum
 * + White     - domain in while list
 * + malware    - domain malicious
 * - malware: String (name of malware)
 */
case class Whois(
    domainName: String,
    registrar: String,
    whoisServer: String,
    referral: String,
    nameServer: Array[String],
    status: String,
    create: String,
    update: String,
    expire: String) {
  def this() = this("", "", "", "", Array[String](), "", "", "", "")
  override def toString = List(
    domainName,
    registrar,
    whoisServer,
    referral,
    nameServer.mkString(","),
    status,
    create,
    update,
    expire).mkString(TAB)

  def isValid(): Boolean = {
    // valid when domainName And Registrar And create And expire is not null or empty
    val flag1 = isNotNullAndEmpty(domainName) && isNotNullAndEmpty(registrar) && isNotNullAndEmpty(create) && isNotNullAndEmpty(expire)
    // valid when domainName And Registrar And whoisServer And referral is not null or empty
    val flag2 = isNotNullAndEmpty(domainName) && isNotNullAndEmpty(registrar) && isNotNullAndEmpty(whoisServer) && isNotNullAndEmpty(referral)

    flag1 || flag2
  }
}

object Whois {

  private val VN_TLD = ".vn"
  private val IT_TLD = ".it"
  private val CZ_TLD = ".cz"
  private val NU_TLD = ".nu"
  private val RU_TLD = ".ru"

  def apply(line: String): Whois = {
    val array = line.split(TAB)
    var index = -1
    def increase() = {
      index = index + 1
      index
    }
    Whois(
      array(increase()),
      array(increase()),
      array(increase()),
      array(increase()),
      array(increase()).split(","),
      array(increase()),
      array(increase()),
      array(increase()),
      array(increase()))
  }

  def apply(content: String, domain: String): Whois = {
    parse(content, domain)
  }

  def crawl(domain: String): Whois = {
    if (StringUtil.isNullOrEmpty(domain)) {
      new Whois()
    } else {
      def f(parser: AbstractParser, domain: String): String = getContent(domain, parser.WHOIS_SERVER)
      val lastIndex = domain.lastIndexOf(".")
      val tld = if (lastIndex > 0 && lastIndex < domain.length()) domain.substring(lastIndex) else domain
      val content = tld match {
        case VN_TLD => f(new VnParser(), domain)
        case IT_TLD => f(new ItParser(), domain)
        case CZ_TLD => f(new CzParser(), domain)
        case NU_TLD => f(new NuParser(), domain)
        case RU_TLD => f(new RuParser(), domain)
        case _      => f(new InternicParser(), domain)
      }
      parse(content, domain)
    }
  }

  private def parse(content: String, domain: String): Whois = {
    if (StringUtil.isNullOrEmpty(domain)) {
      new Whois()
    } else {
      def f(parser: AbstractParser, content: String, domain: String) = parser.newWhois(content, domain)
      val lastIndex = domain.lastIndexOf(".")
      val tld = if (lastIndex > 0 && lastIndex < domain.length()) domain.substring(lastIndex) else domain
      tld match {
        case VN_TLD => f(new VnParser(), content, domain)
        case IT_TLD => f(new ItParser(), content, domain)
        case CZ_TLD => f(new CzParser(), content, domain)
        case NU_TLD => f(new NuParser(), content, domain)
        case RU_TLD => f(new RuParser(), content, domain)
        case _      => f(new InternicParser(), content, domain)
      }
    }
  }

  def getContent(domain: String, host: String): String = {
    val whoisResult = new StringBuilder("");
    val client: WhoisClient = new WhoisClient();
    try {
      client.connect(host);
      val whoisData = client.query(domain);
      whoisResult.append(whoisData);
      client.disconnect();
    } catch {
      case e: SocketException => println(e)
      case e: IOException     => println(e)
      case _: Throwable       => println("Error")
    }
    whoisResult.toString()
  }

  def main(args: Array[String]) {
    val content = """
%  (c) 2006-2017 CZ.NIC, z.s.p.o.
% 
% Intended use of supplied data and information
% 
% Data contained in the domain name register, as well as information
% supplied through public information services of CZ.NIC association,
% are appointed only for purposes connected with Internet network
% administration and operation, or for the purpose of legal or other
% similar proceedings, in process as regards a matter connected
% particularly with holding and using a concrete domain name.
% 
% Full text available at:
% http://www.nic.cz/page/306/intended-use-of-supplied-data-and-information/
% 
% See also a search service at http://www.nic.cz/whois/
% 
% 
% Whoisd Server Version: 3.10.2
% Timestamp: Mon Aug 14 11:42:02 2017

domain:       svist21.cz
registrant:   SB:SVIST21-S
admin-c:      SB:SVIST21-S
nsset:        NSS:GRANSY:3
keyset:       KEYSET
registrar:    REG-GRANSY
registered:   02.02.2001 16:43:00
changed:      17.07.2012 11:28:02
expire:       03.02.2018

contact:      SB:SVIST21-S
org:          Svist 21 s.r.o.
name:         Svist 21 s.r.o.
address:      Dobrovskeho 36
address:      Praha 7
address:      17000
address:      CZ
registrar:    REG-GRANSY
created:      05.10.2005 11:55:00
changed:      30.07.2014 09:47:05

nsset:        NSS:GRANSY:3
nserver:      ns.gransy.com 
nserver:      ns2.gransy.com 
nserver:      ns3.gransy.com 
nserver:      ns4.gransy.com 
nserver:      ns5.gransy.com 
tech-c:       GRANSY
registrar:    REG-GRANSY
created:      01.10.2007 02:00:00
changed:      16.08.2010 00:39:13

contact:      GRANSY
org:          Gransy s.r.o.
name:         Jan Horák
address:      Bořivojova 878/35
address:      Praha 3
address:      130 00
address:      CZ
phone:        +420.732954549
fax-no:       +420.226517341
e-mail:       info@gransy.com
registrar:    REG-MOJEID
created:      23.08.2004 17:35:00
changed:      20.04.2011 14:22:45

keyset:       KEYSET
dnskey:       257 3 7 BQEAAAABw0H2Xb7JjIuMMVRD3oqWpoXsriUK4sCT2B0TAc9b6v7K+gEIfhtrQ+LImQ/yY4VLZ1z88RDe48LvV2kA3fjB+4tFJTsgmgxCAg29skRNorVLnb6ztSqZO3FuTYgH3yywEw3W4rTkPfthNhiaMEVXVrFDDU4dGhiJmvIa9mkaPOkIKeRV4gJqs2YSEIhCKeMxkNNGLn1CIXAiFjVbVDcYFv0n1bBY2iDUllDIRZapMfoSwJMnHI6VXz3CGjxIfcFcr+BUfVFhobqyV848n4HJcHKMgErtC8xFmRD++Pq/isLbNs48zDSZQY5jJvD30anwzZnzhWJJ2ZlirUm6pIazB3a6A7V3c381TsRAyY8suy5pkEriSVs4wSfHkiiwd3Z1sHCTHgefwyRrArFycXR4bvz9sSFOCjbZfJ4S2RFchQa2D+IJsea+kXa+LGOi2enMd6Jaq5+WB6dUkgWz+9a0/xqCC2ShywyWeazuoLaaejL8NUDfsGj4TEHfkXX+/BodFl6SicWsQEZuNU44/+pyyFqgDKsHu9t8mDtz/IGRZ/Duj9GKTQ4j953Czkic0thvFwqqd6Xm+C48K1qIB1vWqV4AinXDVf/qjbkPxGP01P+riUs5E0zTEoJOtyTtm/xoV5lTwe2PvhysrtGmcTdyqZXDZ6DQnUgkO7BUjlprbnk=
tech-c:       GRANSY
registrar:    REG-GRANSY
created:      20.10.2010 18:09:11
changed:      25.10.2010 00:49:05
    """
    //    val domain = "svist21.cz"
    val domain = "tin.it"

    println(Whois.apply(content, domain))
  }
}