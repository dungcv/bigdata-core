package com.ftel.bigdata.whois

import org.apache.commons.net.whois.WhoisClient

/**
 * Parser for Whois Record which get from "whois.internic.net" server
 * @author dungvc [dungvc2@fpt.com.vn]
 */
class InternicParser extends AbstractParser {

  override val WHOIS_SERVER: String = "whois.internic.net"

  override val DOMAIN_NAME_REGEX = ".*.(?i)(Domain Name):\\s(.*.)$".r("k", "v")
  override val REGISTRAR_REGEX = ".*.(?i)(Registrar):\\s(.*.)$".r("k", "v")
  override val WHOIS_SERVER_REGEX = ".*.(?i)(Whois Server):\\s(.*.)$".r("k", "v")
  override val REFERRAL_URL_REGEX = ".*.(?i)(URL):\\s(.*.)$".r("k", "v")
  override val NAME_SERVER_REGEX = ".*.(?i)(Name Server):\\s(.*.)$".r("k", "v")
  override val STATUS_REGEX = ".*.(?i)(Status):\\s(.*.)$".r("k", "v")
  override val UPDATED_DATE_REGEX = ".*.(?i)(Updated Date):\\s(.*.)$".r("k", "v")
  override val CREATION_DATE_REGEX = ".*.(?i)(Creation Date):\\s(.*.)$".r("k", "v")
  //override val EXPIRATION_DATE_REGEX = ".*.(?i)(Expiration Date):\\s(.*.)$".r("k", "v")
  //override val EXPIRATION_DATE_REGEX = ".*.(?i)(Expiry Date):\\s(.*.)$".r("k", "v")
  override val EXPIRATION_DATE_REGEX = ".*.(?i)(Expiry Date|Expiration Date):\\s(.*.)$".r("k", "v")
  override val BEGIN_KEY = "Domain Name:"

  override val SAMPLE = """
      Domain Name: JIASHULE.COM
   Registry Domain ID: 1687425593_DOMAIN_COM-VRSN
   Registrar WHOIS Server: whois.godaddy.com
   Registrar URL: http://www.godaddy.com
   Updated Date: 2014-07-15T07:03:01Z
   Creation Date: 2011-11-16T10:32:04Z
   Registry Expiry Date: 2017-11-16T10:32:04Z
   Registrar: GoDaddy.com, LLC
   Registrar IANA ID: 146
   Registrar Abuse Contact Email: abuse@godaddy.com
   Registrar Abuse Contact Phone: 480-624-2505
   Domain Status: clientDeleteProhibited https://icann.org/epp#clientDeleteProhibited
   Domain Status: clientRenewProhibited https://icann.org/epp#clientRenewProhibited
   Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
   Domain Status: clientUpdateProhibited https://icann.org/epp#clientUpdateProhibited
   Name Server: NS1.JIASULE.NET
   Name Server: NS2.JIASULE.NET
   DNSSEC: unsigned
   URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
    """
}

object InternicParser {
  def main(args: Array[String]) {
    val parser = new InternicParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    println(whois.toString())
    println(whois.isValid())
  }
}