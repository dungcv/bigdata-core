package com.ftel.bigdata.whois

import com.ftel.bigdata.utils.StringUtil

/**
 * @author dungvc [dungvc2@fpt.com.vn]
 */
class ItParser extends AbstractParser {

  override val WHOIS_SERVER = "whois.nic.it"
  
  override val DOMAIN_NAME_REGEX = ".*.(?i)(domain):\\s(.*.)$".r("k", "v")
  override val REGISTRAR_REGEX = ".*.(?i)(org):\\s(.*.)$".r("k", "v")
  override val WHOIS_SERVER_REGEX = ".*.(?i)(Whois Server):\\s(.*.)$".r("k", "v")
  override val REFERRAL_URL_REGEX = ".*.(?i)(URL):\\s(.*.)$".r("k", "v")
  override val NAME_SERVER_REGEX = ".*.(?i)(nserver):\\s(.*.)$".r("k", "v")
  override val STATUS_REGEX = ".*.(?i)(Status):\\s(.*.)$".r("k", "v")
  override val UPDATED_DATE_REGEX = ".*.(?i)(update):\\s(.*.)$".r("k", "v")
  override val CREATION_DATE_REGEX = ".*.(?i)(created):\\s(.*.)$".r("k", "v")
  override val EXPIRATION_DATE_REGEX = ".*.(?i)(expire date):\\s(.*.)$".r("k", "v")
  override val BEGIN_KEY = "domain:"

  override val SAMPLE = """
*********************************************************************
* Please note that the following result could be a subgroup of      *
* the data contained in the database.                               *
*                                                                   *
* Additional information can be visualized at:                      *
* http://www.nic.it/cgi-bin/Whois/whois.cgi                         *
*********************************************************************

Domain:             tin.it
Status:             ok
Created:            1996-10-03 00:00:00
Last Update:        2017-08-07 00:48:23
Expire Date:        2018-07-22

Registrant
  Organization:     Telecom Italia S.p.A.
  Address:          Piazza degli Affari n. 2
                    Milano
                    20124
                    MI
                    IT
  Created:          2016-02-25 15:37:53
  Last Update:      2016-02-25 15:37:53

Admin Contact
  Name:             Francesco Battipede
  Address:          Piazza degli Affari n. 2
                    Milano
                    20124
                    MI
                    IT
  Created:          2013-07-17 12:40:06
  Last Update:      2013-07-17 12:40:06

Technical Contacts
  Name:             Staff Postmaster Tin
  Organization:     Telecom Italia S.p.A.
  Address:          Via C. Colombo, 142
                    Roma
                    00147
                    RM
                    IT
  Created:          2004-05-14 00:00:00
  Last Update:      2011-05-23 10:57:11

  Name:             Staff Tech Tin
  Organization:     Telecom Italia S.p.A.
  Address:          Via C. Colombo, 142
                    Roma
                    00147
                    RM
                    IT
  Created:          2004-05-14 00:00:00
  Last Update:      2011-05-23 10:57:11

  Name:             Staff Zone Tin
  Organization:     Telecom Italia S.p.A.
  Address:          Via C. Colombo, 142
                    Roma
                    00147
                    RM
                    IT
  Created:          2004-05-14 00:00:00
  Last Update:      2011-05-23 10:57:11

Registrar
  Organization:     Telecom Italia s.p.a.
  Name:             TIN-REG
  Web:              http://www.telecomitalia.it

Nameservers
  dns.tin.it
  dnsca.tin.it
    """

  val SAMPLE2 = """

*********************************************************************
* Please note that the following result could be a subgroup of      *
* the data contained in the database.                               *
*                                                                   *
* Additional information can be visualized at:                      *
* http://www.nic.it/cgi-bin/Whois/whois.cgi                         *
*********************************************************************

Domain:             1and1.it
Status:             ok
Created:            2000-07-24 00:00:00
Last Update:        2017-05-14 00:56:00
Expire Date:        2018-04-28

Registrant
  Organization:     1&1 Internet SE
  Address:          Elgendorfer Str. 57
                    Montabaur
                    56410
                    DE
                    DE
  Created:          2016-03-03 14:08:27
  Last Update:      2016-03-03 14:08:27

Admin Contact
  Name:             Henning Kettler
  Organization:     Henning Kettler
  Address:          Elgendorfer Str. 57
                    Montabaur
                    56410
                    DE
                    DE
  Created:          2016-03-03 14:08:27
  Last Update:      2016-03-03 14:08:27

Technical Contacts
  Name:             Hostmaster of the day
  Organization:     Hostmaster of the day
  Address:          Elgendorfer Str. 57
                    Montabaur
                    56410
                    DE
                    DE
  Created:          2016-03-03 14:08:27
  Last Update:      2017-06-22 15:47:53

Registrar
  Organization:     InterNetX GmbH
  Name:             INTERNETXGMBH-REG
  Web:              http://www.internetx.com/

Nameservers
  ns-1and1.ui-dns.com
  ns-1and1.ui-dns.de
  ns-1and1.ui-dns.org
  ns-1and1.ui-dns.biz
      """
  override def newWhois(content: String, domain: String): Whois = {
    val info = content.substring(0, content.indexOf("Registrant"))
    val registrarKey = content.substring(content.indexOf("Registrar"), content.indexOf("Nameservers"))
    val nameServerKey = content.substring(content.indexOf("Nameservers"))

    val infoLines = info.split("\n").map(x => x.trim())
    val domainName = if (domain == null) infoLines.find(x => x.startsWith("Domain:")).getOrElse("").substring(7).trim() else domain
    val status = infoLines.find(x => x.startsWith("Status:")).getOrElse("").substring(7).trim()

    def findValueInPattern = (lines: Array[String], pattern: String) => {
      val value = lines.find(x => x.startsWith(pattern)).getOrElse("")
      if (StringUtil.isNullOrEmpty(value)) value else value.substring(pattern.length()).trim()
    }
    def nomalizeDate = (x: String) => {
      x.split(" ")(0)
    }

    val create = nomalizeDate(findValueInPattern(infoLines, "Created:"))
    val update = nomalizeDate(findValueInPattern(infoLines, "Last Update:")) // nomalizeDate(infoLines.find(x => x.startsWith("Last Update:")).getOrElse("").substring(12).trim())
    val expire = nomalizeDate(findValueInPattern(infoLines, "Expire Date:")) // nomalizeDate(infoLines.find(x => x.startsWith("Expire Date:")).getOrElse("").substring(12).trim())

    val registrarLines = registrarKey.split("\n").map(x => x.trim())
    //val registrar = getValue(registrarLines.find(x => x.startsWith("Organization:")).getOrElse(""))

    val registrar = findValueInPattern(registrarLines, "Organization:")
    //val orgInfo = registrarLines.find(x => x.startsWith("Organization:")).getOrElse("").substring(7).trim()
    val url = findValueInPattern(registrarLines, "Web:") // .find(x => x.startsWith("Web:")).getOrElse("").substring(4).trim()
    val whoisServer = if (domainName.endsWith(".it")) {
      "whois.nic.it"
    } else url
    val nameServers = nameServerKey.split("\n").map(x => x.trim()).filter(x => x != "Nameservers" && StringUtil.isNotNullAndEmpty(x))
    Whois(domainName, registrar, whoisServer, url, nameServers, status, create, update, expire)
  }

  private def getValue(s: String): String = {
    val index = s.indexOf(":") + 1
    if (index > 1) {
      s.split(":").drop(1).mkString(":").trim()
    } else ""
  }
}

object ItParser {
  def main(args: Array[String]) {
    val parser = new ItParser()
    val whois = parser.newWhois(parser.SAMPLE2, null)
    println(whois.toString())
    println(whois.isValid())

    println(whois.domainName.split("\\.").mkString(" "))
    println(whois.domainName.substring(whois.domainName.lastIndexOf(".")))
  }
}