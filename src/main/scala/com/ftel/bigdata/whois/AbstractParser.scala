package com.ftel.bigdata.whois

import scala.util.matching.Regex
import com.ftel.bigdata.utils.StringUtil

abstract class AbstractParser {

  val WHOIS_SERVER: String

  val DOMAIN_NAME_REGEX: Regex
  val REGISTRAR_REGEX: Regex
  val WHOIS_SERVER_REGEX: Regex
  val REFERRAL_URL_REGEX: Regex
  val NAME_SERVER_REGEX: Regex
  val STATUS_REGEX: Regex
  val UPDATED_DATE_REGEX: Regex
  val CREATION_DATE_REGEX: Regex
  val EXPIRATION_DATE_REGEX: Regex

  val BEGIN_KEY: String

  val SAMPLE: String

  private val DOMAIN_NAME_KEY = "DomainName"
  private val REGISTRAR_KEY = "Registrar"
  private val WHOIS_SERVER_KEY = "WhoisServer"
  private val REFERRAL_URL_KEY = "Referral"
  private val NAME_SERVER_KEY = "NameServer"
  private val STATUS_KEY = "Status"
  private val UPDATED_DATE_KEY = "Update"
  private val CREATION_DATE_KEY = "Create"
  private val EXPIRATION_DATE_KEY = "Expiration"

  def newWhois(content: String, domain: String): Whois = {
    newWhois(content.split("\n"), domain)
  }

  private def newWhois(lines: Array[String], domain: String): Whois = {
    val from = lines.indexWhere(x => x.toLowerCase().contains((s"${BEGIN_KEY} ${domain}").toLowerCase()))

    val linesTemp = (if (from < 0 || from > lines.size) lines else lines.slice(from, lines.size)).map(x => " " + x.trim())
    val kv = linesTemp.map(x => getKeyValue(x)).filter(x => x != null)

    val domainName = if (domain == null) getValue(DOMAIN_NAME_KEY, kv) else domain
    val registrar = getValue(REGISTRAR_KEY, kv)
    val whoisServer = getValue(WHOIS_SERVER_KEY, kv)
    val referral = getValue(REFERRAL_URL_KEY, kv)
    val nameServers = getValue(NAME_SERVER_KEY, kv).split(",")
    val status = nomalize(getValue(STATUS_KEY, kv))
    def nomalizeDate = (x: String) => x.split("t")(0)
    val update = nomalizeDate(getValue(UPDATED_DATE_KEY, kv))
    val create = nomalizeDate(getValue(CREATION_DATE_KEY, kv))
    val expire = nomalizeDate(getValue(EXPIRATION_DATE_KEY, kv))

    Whois(domainName, registrar, whoisServer, referral, nameServers, status, create, update, expire)
  }

  private val UNKOWN = "unknow" -> 0
  private val OK = "ok" -> 1
  private val PROHIBITED = "prohibited" -> 2
  private val HOLD = "hold" -> 3
  private val REDEMPTION = "redemption" -> 4

  def getId(status: String): Int = {
    val stringWithoutSpace = StringUtil.removeAllWhiteSpace(status).toLowerCase()
    stringWithoutSpace match {
      case OK._1         => OK._2
      case PROHIBITED._1 => PROHIBITED._2
      case HOLD._1       => HOLD._2
      case REDEMPTION._1 => REDEMPTION._2
      case _             => UNKOWN._2
    }
  }

  def nomalize(status: String): String = {
    val stringWithoutSpace = StringUtil.removeAllWhiteSpace(status).toLowerCase()
    if (stringWithoutSpace.contains(OK._1)) OK._1
    else if (stringWithoutSpace.contains(PROHIBITED._1)) PROHIBITED._1
    else if (stringWithoutSpace.contains(HOLD._1)) HOLD._1
    else if (stringWithoutSpace.contains(REDEMPTION._1)) REDEMPTION._1
    else status
  }

  private def getValue(key: String, list: Array[(String, String)]): String = {
    list.filter(x => x._1 == key).map(x => x._2.toLowerCase().trim()).mkString(",")
  }

  private def getKeyValue(line: String): (String, String) = {
    val res = line match {
      case DOMAIN_NAME_REGEX(k, v)     => DOMAIN_NAME_KEY -> v
      case REGISTRAR_REGEX(k, v)       => REGISTRAR_KEY -> v
      case WHOIS_SERVER_REGEX(k, v)    => WHOIS_SERVER_KEY -> v
      case REFERRAL_URL_REGEX(k, v)    => REFERRAL_URL_KEY -> v
      case NAME_SERVER_REGEX(k, v)     => NAME_SERVER_KEY -> v
      case STATUS_REGEX(k, v)          => STATUS_KEY -> v
      case UPDATED_DATE_REGEX(k, v)    => UPDATED_DATE_KEY -> v
      case CREATION_DATE_REGEX(k, v)   => CREATION_DATE_KEY -> v
      case EXPIRATION_DATE_REGEX(k, v) => EXPIRATION_DATE_KEY -> v
      case _                           => null
    }
    res
  }
}
