package com.ftel.bigdata.whois

import com.ftel.bigdata.utils.StringUtil
import com.ftel.bigdata.utils.DateTimeUtil

/**
 * @author dungvc [dungvc2@fpt.com.vn]
 */
class CzParser extends AbstractParser {

  override val WHOIS_SERVER: String = "whois.nic.cz"

  override val DOMAIN_NAME_REGEX = ".*.(?i)(domain):\\s(.*.)$".r("k", "v")
  override val REGISTRAR_REGEX = ".*.(?i)(org):\\s(.*.)$".r("k", "v")
  override val WHOIS_SERVER_REGEX = ".*.(?i)(Whois Server):\\s(.*.)$".r("k", "v")
  override val REFERRAL_URL_REGEX = ".*.(?i)(URL):\\s(.*.)$".r("k", "v")
  override val NAME_SERVER_REGEX = ".*.(?i)(nserver):\\s(.*.)$".r("k", "v")
  override val STATUS_REGEX = ".*.(?i)(Status):\\s(.*.)$".r("k", "v")
  override val UPDATED_DATE_REGEX = ".*.(?i)(update):\\s(.*.)$".r("k", "v")
  override val CREATION_DATE_REGEX = ".*.(?i)(created):\\s(.*.)$".r("k", "v")
  override val EXPIRATION_DATE_REGEX = ".*.(?i)(expire date):\\s(.*.)$".r("k", "v")
  override val BEGIN_KEY = "domain:"

  override val SAMPLE = """
%  (c) 2006-2017 CZ.NIC, z.s.p.o.
% 
% Intended use of supplied data and information
% 
% Data contained in the domain name register, as well as information
% supplied through public information services of CZ.NIC association,
% are appointed only for purposes connected with Internet network
% administration and operation, or for the purpose of legal or other
% similar proceedings, in process as regards a matter connected
% particularly with holding and using a concrete domain name.
% 
% Full text available at:
% http://www.nic.cz/page/306/intended-use-of-supplied-data-and-information/
% 
% See also a search service at http://www.nic.cz/whois/
% 
% 
% Whoisd Server Version: 3.10.2
% Timestamp: Mon Aug 14 11:42:02 2017

domain:       svist21.cz
registrant:   SB:SVIST21-S
admin-c:      SB:SVIST21-S
nsset:        NSS:GRANSY:3
keyset:       KEYSET
registrar:    REG-GRANSY
registered:   02.02.2001 16:43:00
changed:      17.07.2012 11:28:02
expire:       03.02.2018

contact:      SB:SVIST21-S
org:          Svist 21 s.r.o.
name:         Svist 21 s.r.o.
address:      Dobrovskeho 36
address:      Praha 7
address:      17000
address:      CZ
registrar:    REG-GRANSY
created:      05.10.2005 11:55:00
changed:      30.07.2014 09:47:05

nsset:        NSS:GRANSY:3
nserver:      ns.gransy.com 
nserver:      ns2.gransy.com 
nserver:      ns3.gransy.com 
nserver:      ns4.gransy.com 
nserver:      ns5.gransy.com 
tech-c:       GRANSY
registrar:    REG-GRANSY
created:      01.10.2007 02:00:00
changed:      16.08.2010 00:39:13

contact:      GRANSY
org:          Gransy s.r.o.
name:         Jan Horák
address:      Bořivojova 878/35
address:      Praha 3
address:      130 00
address:      CZ
phone:        +420.732954549
fax-no:       +420.226517341
e-mail:       info@gransy.com
registrar:    REG-MOJEID
created:      23.08.2004 17:35:00
changed:      20.04.2011 14:22:45

keyset:       KEYSET
dnskey:       257 3 7 BQEAAAABw0H2Xb7JjIuMMVRD3oqWpoXsriUK4sCT2B0TAc9b6v7K+gEIfhtrQ+LImQ/yY4VLZ1z88RDe48LvV2kA3fjB+4tFJTsgmgxCAg29skRNorVLnb6ztSqZO3FuTYgH3yywEw3W4rTkPfthNhiaMEVXVrFDDU4dGhiJmvIa9mkaPOkIKeRV4gJqs2YSEIhCKeMxkNNGLn1CIXAiFjVbVDcYFv0n1bBY2iDUllDIRZapMfoSwJMnHI6VXz3CGjxIfcFcr+BUfVFhobqyV848n4HJcHKMgErtC8xFmRD++Pq/isLbNs48zDSZQY5jJvD30anwzZnzhWJJ2ZlirUm6pIazB3a6A7V3c381TsRAyY8suy5pkEriSVs4wSfHkiiwd3Z1sHCTHgefwyRrArFycXR4bvz9sSFOCjbZfJ4S2RFchQa2D+IJsea+kXa+LGOi2enMd6Jaq5+WB6dUkgWz+9a0/xqCC2ShywyWeazuoLaaejL8NUDfsGj4TEHfkXX+/BodFl6SicWsQEZuNU44/+pyyFqgDKsHu9t8mDtz/IGRZ/Duj9GKTQ4j953Czkic0thvFwqqd6Xm+C48K1qIB1vWqV4AinXDVf/qjbkPxGP01P+riUs5E0zTEoJOtyTtm/xoV5lTwe2PvhysrtGmcTdyqZXDZ6DQnUgkO7BUjlprbnk=
tech-c:       GRANSY
registrar:    REG-GRANSY
created:      20.10.2010 18:09:11
changed:      25.10.2010 00:49:05
    """
  override def newWhois(content: String, domain: String): Whois = {
    val lines = content.split("\n").filter(x => !x.startsWith("%")).map(x => x.trim())
    val domainName = if (domain == null) getValue(lines.find(x => x.startsWith("domain:")).getOrElse("")) else domain
    val registrar = getValue(lines.find(x => x.startsWith("registrant:")).getOrElse(""))
    val create = getValue(lines.find(x => x.startsWith("registered:")).getOrElse(""))
    val update = getValue(lines.find(x => x.startsWith("changed:")).getOrElse(""))
    val expire = getValue(lines.find(x => x.startsWith("expire:")).getOrElse(""))
    val whoisServer = if (domainName.endsWith(".cz")) {
      "whois.nic.cz"
    } else ""
    val nameServers = lines.filter(x => x.startsWith("nserver"))
      .map(x => getValue(x))
      .filter(x => !x.isEmpty())

    Whois(domainName, registrar, whoisServer, "", nameServers, "unknow", nomalizeDate(create), nomalizeDate(update), nomalizeDate(expire))
  }

  private def getValue(s: String): String = {
    val index = s.indexOf(":") + 1
    if (index > 1) {
      s.split(":").drop(1).mkString(":").trim()
    } else ""
  }

  private def nomalizeDate(date: String): String = {
    val day = date.split(" ").apply(0)
    DateTimeUtil.create(day, "dd.MM.yyyy").toString(DateTimeUtil.YMD)
  }
}

object CzParser {
  def main(args: Array[String]) {
    val parser = new CzParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    println(whois.toString())
    println(whois.isValid())
  }
}