package com.ftel.bigdata.whois

/**
 * @author dungvc [dungvc2@fpt.com.vn]
 */
class NuParser extends AbstractParser {

  override val WHOIS_SERVER: String = "whois.internic.net"

  override val DOMAIN_NAME_REGEX = ".*.(?i)(domain):\\s(.*.)$".r("k", "v")
  override val REGISTRAR_REGEX = ".*.(?i)(registrar):\\s(.*.)$".r("k", "v")
  override val WHOIS_SERVER_REGEX = ".*.(?i)(Whois Server):\\s(.*.)$".r("k", "v")
  override val REFERRAL_URL_REGEX = ".*.(?i)(URL):\\s(.*.)$".r("k", "v")
  override val NAME_SERVER_REGEX = ".*.(?i)(nserver):\\s(.*.)$".r("k", "v")
  override val STATUS_REGEX = ".*.(?i)(status):\\s(.*.)$".r("k", "v")
  override val UPDATED_DATE_REGEX = ".*.(?i)(modified):\\s(.*.)$".r("k", "v")
  override val CREATION_DATE_REGEX = ".*.(?i)(created):\\s(.*.)$".r("k", "v")
  override val EXPIRATION_DATE_REGEX = ".*.(?i)(expires):\\s(.*.)$".r("k", "v")
  override val BEGIN_KEY = "domain:"

  override val SAMPLE = """
    # All rights reserved.
    # The information obtained through searches, or otherwise, is protected
    # by the Swedish Copyright Act (1960:729) and international conventions.
    # It is also subject to database protection according to the Swedish
    # Copyright Act.
    # Any use of this material to target advertising or
    # similar activities is forbidden and will be prosecuted.
    # If any of the information below is transferred to a third
    # party, it must be done in its entirety. This server must
    # not be used as a backend for a search engine.
    # Result of search for registered domain names under
    # the .nu top level domain.
    # This whois printout is printed with UTF-8 encoding.
    #
    state:            active
    domain:           activum.nu
    holder:           toract8446-00001
    admin-c:          -
    tech-c:           -
    billing-c:        -
    created:          2003-01-14
    modified:         2016-11-29
    expires:          2018-01-14
    transferred:      2015-08-06
    nserver:          ns.dnsdrift.net
    nserver:          ns2.dnsdrift.net
    dnssec:           unsigned delegation
    status:           ok
    registrar:        Loopia AB
    """
}

object NuParser {
  def main(args: Array[String]) {
    val parser = new NuParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    println(whois.toString())
    println(whois.isValid())
  }
}