package com.ftel.bigdata.whois

/**
 * @author dungvc [dungvc2@fpt.com.vn]
 */
class RuParser extends AbstractParser {

  override val WHOIS_SERVER: String = "whois.internic.net"

  override val DOMAIN_NAME_REGEX = ".*.(?i)(domain):\\s(.*.)$".r("k", "v")
  override val REGISTRAR_REGEX = ".*.(?i)(org):\\s(.*.)$".r("k", "v")
  override val WHOIS_SERVER_REGEX = ".*.(?i)(Whois Server):\\s(.*.)$".r("k", "v")
  override val REFERRAL_URL_REGEX = ".*.(?i)(URL):\\s(.*.)$".r("k", "v")
  override val NAME_SERVER_REGEX = ".*.(?i)(nserver):\\s(.*.)$".r("k", "v")
  override val STATUS_REGEX = ".*.(?i)(state):\\s(.*.)$".r("k", "v")
  override val UPDATED_DATE_REGEX = ".*.(?i)(paid-till):\\s(.*.)$".r("k", "v")
  override val CREATION_DATE_REGEX = ".*.(?i)(created):\\s(.*.)$".r("k", "v")
  override val EXPIRATION_DATE_REGEX = ".*.(?i)(free-date):\\s(.*.)$".r("k", "v")
  override val BEGIN_KEY = "domain:"

  override val SAMPLE = """
    domain:        NM.RU
    nserver:       ns2.itmm.ru.
    nserver:       ns3.itmm.ru.
    state:         REGISTERED, DELEGATED, VERIFIED
    org:           Media Mir Ltd.
    registrar:     SALENAMES-RU
    admin-contact: https://partner.salenames.ru/contact_admin.khtml
    created:       2000-02-03T07:07:46Z
    paid-till:     2018-02-28T21:00:00Z
    free-date:     2018-04-01
    source:        TCI
    """
}

object RuParser {
  def main(args: Array[String]) {
    val parser = new RuParser()
    val whois = parser.newWhois(parser.SAMPLE, null)
    println(whois.toString())
    println(whois.isValid())
  }
}