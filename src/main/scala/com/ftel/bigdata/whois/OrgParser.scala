package com.ftel.bigdata.whois

/**
 * @author dungvc [dungvc2@fpt.com.vn]
 */
class OrgParser extends AbstractParser {

  override val WHOIS_SERVER: String = "whois.internic.net"

  override val DOMAIN_NAME_REGEX = ".*.(Domain Name):\\s(.*.)$".r("k", "v")
  override val REGISTRAR_REGEX = ".*.(Registrar):\\s(.*.)$".r("k", "v")
  override val WHOIS_SERVER_REGEX = ".*.(Registrar WHOIS Server):\\s(.*.)$".r("k", "v")
  override val REFERRAL_URL_REGEX = ".*.(Registrar URL):\\s(.*.)$".r("k", "v")
  override val NAME_SERVER_REGEX = ".*.(Name Server):\\s(.*.)$".r("k", "v")
  override val STATUS_REGEX = ".*.(Domain Status):\\s(.*.)$".r("k", "v")
  override val UPDATED_DATE_REGEX = ".*.(Updated Date):\\s(.*.)$".r("k", "v")
  override val CREATION_DATE_REGEX = ".*.(Creation Date):\\s(.*.)$".r("k", "v")
  override val EXPIRATION_DATE_REGEX = ".*.(Registry Expiry Date):\\s(.*.)$".r("k", "v")
  override val BEGIN_KEY = "Domain Name:"

  override val SAMPLE = """
    Domain Name: WIKIPEDIA.ORG
    Registry Domain ID: D51687756-LROR
    Registrar WHOIS Server:
    Registrar URL: http://www.markmonitor.com
    Updated Date: 2015-12-12T10:16:19Z
    Creation Date: 2001-01-13T00:12:14Z
    Registry Expiry Date: 2023-01-13T00:12:14Z
    Registrar Registration Expiration Date:
    Registrar: MarkMonitor Inc.
    Registrar IANA ID: 292
    Registrar Abuse Contact Email:
    Registrar Abuse Contact Phone:
    Reseller:
    Domain Status: clientDeleteProhibited https://icann.org/epp#clientDeleteProhibited
    Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
    Domain Status: clientUpdateProhibited https://icann.org/epp#clientUpdateProhibited
    Registry Registrant ID: C121149869-LROR
    Registrant Name: Domain Admin
    Registrant Organization: Wikimedia Foundation, Inc.
    Registrant Street: 149 New Montgomery Street
    Registrant Street: Third Floor
    Registrant City: San Francisco
    Registrant State/Province: CA
    Registrant Postal Code: 94105
    Registrant Country: US
    Registrant Phone: +1.4158396885
    Registrant Phone Ext:
    Registrant Fax: +1.4158820495
    Registrant Fax Ext:
    Registrant Email: dns-admin@wikimedia.org
    Registry Admin ID: C121149869-LROR
    Admin Name: Domain Admin
    Admin Organization: Wikimedia Foundation, Inc.
    Admin Street: 149 New Montgomery Street
    Admin Street: Third Floor
    Admin City: San Francisco
    Admin State/Province: CA
    Admin Postal Code: 94105
    Admin Country: US
    Admin Phone: +1.4158396885
    Admin Phone Ext:
    Admin Fax: +1.4158820495
    Admin Fax Ext:
    Admin Email: dns-admin@wikimedia.org
    Registry Tech ID: C121149869-LROR
    Tech Name: Domain Admin
    Tech Organization: Wikimedia Foundation, Inc.
    Tech Street: 149 New Montgomery Street
    Tech Street: Third Floor
    Tech City: San Francisco
    Tech State/Province: CA
    Tech Postal Code: 94105
    Tech Country: US
    Tech Phone: +1.4158396885
    Tech Phone Ext:
    Tech Fax: +1.4158820495
    Tech Fax Ext:
    Tech Email: dns-admin@wikimedia.org
    Name Server: NS0.WIKIMEDIA.ORG
    Name Server: NS1.WIKIMEDIA.ORG
    Name Server: NS2.WIKIMEDIA.ORG
    """
}