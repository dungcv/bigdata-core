package com.ftel.bigdata.whois

import com.ftel.bigdata.utils.StringUtil
import com.ftel.bigdata.utils.DateTimeUtil
import com.ftel.bigdata.utils.JsonObject
import play.api.libs.json.JsArray
import scalaj.http.Http

/**
 * @author dungvc [dungvc2@fpt.com.vn]
 */
class VnParser extends AbstractParser {

  override val WHOIS_SERVER: String = "whois.matbao.vn"

  override val DOMAIN_NAME_REGEX = ".*.(?i)(domain):\\s(.*.)$".r("k", "v")
  override val REGISTRAR_REGEX = ".*.(?i)(org):\\s(.*.)$".r("k", "v")
  override val WHOIS_SERVER_REGEX = ".*.(?i)(Whois Server):\\s(.*.)$".r("k", "v")
  override val REFERRAL_URL_REGEX = ".*.(?i)(URL):\\s(.*.)$".r("k", "v")
  override val NAME_SERVER_REGEX = ".*.(?i)(nserver):\\s(.*.)$".r("k", "v")
  override val STATUS_REGEX = ".*.(?i)(Status):\\s(.*.)$".r("k", "v")
  override val UPDATED_DATE_REGEX = ".*.(?i)(update):\\s(.*.)$".r("k", "v")
  override val CREATION_DATE_REGEX = ".*.(?i)(created):\\s(.*.)$".r("k", "v")
  override val EXPIRATION_DATE_REGEX = ".*.(?i)(expire date):\\s(.*.)$".r("k", "v")
  override val BEGIN_KEY = "domain:"

  override val SAMPLE = """
{
  "code": "0",
  "lastUpdated": "06-09-2017 04:18:43",
  "totalQuery": 0,
  "domainName": "tvplay.vn",
  "suffix": "vn",
  "registrar": "Công ty Cổ phần Mắt Bão",
  "nameServer": [
    "ns1.matbao.vn",
    "ns2.matbao.vn"
  ],
  "status": [
    "clienttransferprohibited"
  ],
  "creationDate": "20-04-2015",
  "expirationDate": "20-04-2021",
  "creationDate_L": 1429462800000,
  "expirationDate_L": 1618851600000,
  "registrantName": "Công ty Cổ phần Dịch vụ Truyền thông Vietnamnet Icom",
  "rawtext": "\u003chtml\u003e\u003chead\u003e    \u003ctitle\u003eCheck the existing of a domain\u003c/title\u003e    \u003cmeta http-equiv\u003d\"Content-Type\" content\u003d\"text/HTML; charset\u003dUTF-8\"\u003e\u003c/meta\u003e\u003c/head\u003e\u003cbody\u003etvplay.vn : Record found !\u003cbr\u003eDomain : tvplay.vn\u003cbr\u003eStatus : clientTransferProhibited\u003cbr\u003eIssue Date : 2015-04-20 00:00:00.0\u003cbr\u003eExpired Date : 2021-04-20 00:00:00.0\u003cbr\u003eRegistrar Name : Công ty Cổ phần Mắt Bão\u003cbr\u003eOwner Name : Công ty Cổ phần Dịch vụ Truyền thông Vietnamnet Icom\u003cbr\u003eDNS : ns1.matbao.vn | , ns2.matbao.vn | \u003cbr\u003e\u003cbr\u003e\u003c/body\u003e\u003c/html\u003e"
  }
    """

  // API for get whois with tld is .vn
  private val VN_WHOIS_API_URL = "https://dms.inet.vn/api/public/whois/v1/whois/directly"

  override def newWhois(content: String, domain: String): Whois = {
    val json = new JsonObject(content)
    val domainName = if (domain == null) json.getValueAsString("domainName") else domain
    val registrar = json.getValueAsString("registrar")
    val whoisServer = WHOIS_SERVER
    val referral = WHOIS_SERVER
    val nameServer = json.getValue("nameServer").asInstanceOf[JsArray].value.map(x => x.toString()).map(x => x.replaceAll("\"", "")).toArray
    val status = json.getValue("status").asInstanceOf[JsArray].value.map(x => x.toString()).map(x => x.replaceAll("\"", "")).mkString(",")
    val create = nomalizeDate(json.getValueAsString("creationDate"))
    val expire = nomalizeDate(json.getValueAsString("expirationDate"))
    Whois(domainName, registrar, whoisServer, referral, nameServer, status, create, create, expire)
  }

  private def nomalizeDate(date: String): String = {
    DateTimeUtil.create(date, "dd-MM-yyyy").toString(DateTimeUtil.YMD)
  }

  def getContentFromApi(domain: String): String = {
    getContentFromApi(domain, null, 0)
  }

  def getContentFromApi(domain: String, proxyHost: String, proxyPort: Int): String = {
    val req = if (proxyHost != null)
      Http(VN_WHOIS_API_URL)
        .proxy(proxyHost, proxyPort)
        .postForm(Seq("domainName" -> domain))
    else
      Http(VN_WHOIS_API_URL)
        .postForm(Seq("domainName" -> domain))
    req.asString.body
  }
}

object VnParser {
  def main(args: Array[String]) {
    val parser = new VnParser()
    val whois = parser.newWhois(parser.SAMPLE, "tvplay.vn")
    println(whois.toString())
    println(whois.isValid())
  }
}