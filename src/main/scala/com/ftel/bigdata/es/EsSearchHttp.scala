package com.ftel.bigdata.es

import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.search.SearchResponse
import com.sksamuel.elastic4s.http.search.MultiSearchResponse

// circe
//import com.sksamuel.elastic4s.circe._
//import io.circe.generic.auto._ 

class EsSearchHttp(host: String, port: Int) {
  
  def this(host: String) = this(host, 9200)
  private val client = HttpClient(ElasticsearchClientUri(host, 9200))

  def searchWithType(esIndex: String, esType: String, query: String): SearchResponse = {
    client.execute {
      search(esIndex / esType).query(query)
    }.await
  }
  
  def searchWithoutType(esIndex: String, query: String): SearchResponse = {
    client.execute {
      search(esIndex).query(query)
    }.await
  }
  
  def searchWithBool(esIndex: String, esType: String, field: String, value: String) = {
    client.execute {
      search(esIndex / esType) query {
        {
          must(termQuery(field, value))
          //must(regexQuery("name", ".*cester"),termQuery("status", "Awesome"))
          //not ( termQuery("weather", "hot") )
        }
      }
    }.await
  }

  def multiSearch(esIndex: String, esType: String): MultiSearchResponse = {
    client.execute(
      multi( // use ( not { as we are passing in var args, not a code block
        search(esIndex / esType) query "mylo", // note the trailing comma, we are invoking a var args method
        search(esIndex / esType) query "viva")
    ).await
  }
  
//  search in "places"->"cities" query { bool { must( regexQuery("name", ".*cester"), termQuery("status", "Awesome") ) not ( termQuery("weather", "hot") ) } }
  
  def close() = client.close()
  
}

object EsSearchHttp {
  def main(args: Array[String]) {
    val es = new EsSearchHttp("172.27.11.156")
    val response = es.searchWithBool("dns-service-domain-*", "domain", "domain", "google.com")
    println(response.totalHits)
    es.close()
  }
}