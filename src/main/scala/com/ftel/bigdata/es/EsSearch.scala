package com.ftel.bigdata.es

import com.sksamuel.elastic4s.{ElasticsearchClientUri, TcpClient}
import com.sksamuel.elastic4s.ElasticDsl._

// circe
//import com.sksamuel.elastic4s.circe._
//import io.circe.generic.auto._ 

class EsSearch(host: String, port: Int) {
  
  def this(host: String) = this(host, 9300)
  val client = TcpClient.transport(ElasticsearchClientUri(host, port))

  def search1(esIndex: String, esType: String, query: String) {
    val res = client.execute {
      search(esIndex / esType).query(query)
    }.await
  }
}
