val unit = "junit" % "junit" % "4.4" % "test"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.0" % "test"
val log4j = "log4j" % "log4j" % "1.2.17"
val jodatime = "joda-time" % "joda-time" % "2.9.9"
val esSpark = "org.elasticsearch" % "elasticsearch-spark-20_2.11" % "5.0.0-alpha5"

val commonNet = "commons-net" % "commons-net" % "3.6"
val http = "org.scalaj" % "scalaj-http_2.11" % "2.3.0"
// Slick
val postgres = "org.postgresql" % "postgresql" % "42.0.0"
val slick = "com.typesafe.slick" % "slick_2.11" % "3.2.0"
val hikaricp = "com.typesafe.slick" % "slick-hikaricp_2.11" % "3.2.0"
val esTransport = "org.elasticsearch.client" % "transport" % "5.5.1"

val elastic4sVersion = "5.4.0"
val es4s = "com.sksamuel.elastic4s" %% "elastic4s-core" % elastic4sVersion
val es4sTCP = "com.sksamuel.elastic4s" %% "elastic4s-tcp" % elastic4sVersion
val es4sHTTP = "com.sksamuel.elastic4s" %% "elastic4s-http" % elastic4sVersion

val json = "com.typesafe.play" %% "play-json" % "2.4.8"

val langdect = "com.optimaize.languagedetector" % "language-detector" % "0.6"

lazy val commonSettings = Seq(
  organization := "com.ftel",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8"
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    name := "bigdata-core",
    libraryDependencies ++= Seq(unit, scalaTest, log4j, jodatime, esSpark, commonNet, http, postgres, slick, hikaricp, es4s, es4sTCP, es4sHTTP, json, langdect)
)

assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}

assemblyExcludedJars in assembly := { 
  val cp = (fullClasspath in assembly).value
  cp filter {_.data.getName == "scala-library.jar"}
  cp filter {_.data.getName == "junit-3.8.1.jar"}
}

//resolvers += Resolver.mavenLocal
